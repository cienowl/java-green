package hw.yonghojoon;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

class FileInOut {
	HashMap<String, UserDatabase> userDb = new HashMap<String, UserDatabase>();	//HashMap userDb객체 생성
	
	FileOutputStream fileOut = null;
	FileInputStream fileIn = null;
	
	public FileInOut() {		//클래스 호출시 생성자 실행
		String initStr = "-,-,-,-,-\n";
		
		try {
//			if(fileOut.) 파일 존재 유무 확인 필요
			fileOut = new FileOutputStream("d:/database/user_database.txt");
			
			byte[] b = initStr.getBytes();
			fileOut.write(b);
			
		} catch(Exception e) {
			System.out.println(e + " => 오류");
		} finally {
			try {
				fileOut.close();
			} catch(IOException e) {
				System.out.println(e + "+ close fail");
			}
		}		
	}
	
	public HashMap<String, UserDatabase> readDatabase() {
		try {
			fileIn = new FileInputStream("d:/database/user_database.txt");
			byte[] byteData = new byte[fileIn.available()];
			
			while(fileIn.read(byteData) != -1) {
			}
			
			String keyValue = new String(byteData);
			
			String[] line = keyValue.split("\n");
			
			for(int i = 0; i<line.length; i++) {
				String[] info = line[i].split(",");
				UserDatabase input = new UserDatabase(); 
				
				for(int j = 0; j<info.length; j++) {		
					switch(j) {
					case 0:
						input.setUserId(info[0]);
						break;
					case 1:
						input.setPassword(info[1]);
						break;
					case 2:
						input.setName(info[2]);
						break;
					case 3:
						input.setBirthDate(info[3]);
						break;
					case 4:
						input.setSex(info[4]);
						break;
					}
					userDb.put(info[0], input);
				}
			
			}
					
		} catch(FileNotFoundException e) {
			System.out.println(e + " => 파일을 찾을 수 없습니다.");
        } catch(Exception e) {
        	System.out.println(e + " => 오류");
        } finally {
        	try {
        		fileIn.close();
        	} catch(IOException e) {
        		System.out.println(e + "+ close fail");
        	}
        }
		
		return userDb;
	}
	
	public void writeDatabase(HashMap<String, UserDatabase> userMap) {
		userDb = userMap;		//매개변수로 받아온 HashMap 복사
		
		try {
			fileOut = new FileOutputStream("d:/database/user_database.txt");
			
			Iterator<String> mapIter = userDb.keySet().iterator();
	        while(mapIter.hasNext()) {
	        	String keyValue = null;
	 
	            String key = mapIter.next();
	            UserDatabase value = userDb.get(key);
	 
	            keyValue = value.getUserId() + "," + value.getPassword() + "," + value.getName() + "," + value.getBirthDate() + "," + value.getSex() + "\n"; 
	            
	            byte[] byteData = keyValue.getBytes();
	            fileOut.write(byteData);	 
	        }
		} catch(Exception e) {
			System.out.println(e + " => 오류");
		} finally {
			try {
				fileOut.close();
			} catch(IOException e) {
				System.out.println(e + "+ close fail");
			}
		}
		
	}
	
}