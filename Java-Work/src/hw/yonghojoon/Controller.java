package hw.yonghojoon;

import java.util.HashMap;
import java.util.Scanner;

class Controller {
	int selectNum=0;
	
	HashMap<String, UserDatabase> userMap = new HashMap<String, UserDatabase>();
	FileInOut io = new FileInOut();
	Scanner sc = new Scanner(System.in);		//sc 스캐너 클래스 객체 생성
	
	public void getDatabase() {
		userMap = io.readDatabase();
	}
	
	public void setDatabase() {
		io.writeDatabase(userMap);
	}
	

	public boolean setInfo() {		//회원 정보 저장 메소드
		
		String userId;
		UserDatabase input = new UserDatabase(); //UserDatabae 클래스 객체 생성
		
		System.out.println();
		System.out.println("======== Sign up ========");
		System.out.print("ID: ");
		userId = sc.next();				//userId 변수에 문자열을 입력 받아서 저장
		
		if(userMap.containsKey(userId) == false) {	//userId 값이 userMap HashMap에 key로 등록 되어있으면 (true)
			input.setUserId(userId);
			System.out.print("Password: ");
			input.setPassword(sc.next());	//문자열 입력받아서 input객체를 이용해 setPassword 메소드 호출
			System.out.print("Name: ");
			input.setName(sc.next());		//문자열 입력받아서 input객체를 이용해 setName 메소드 호출
			System.out.print("Birthday(yymmdd): ");
			input.setBirthDate(sc.next());	//문자열 입력받아서 input객체를 이용해 setBirthDate 메소드 호출
			System.out.print("Sex(M/F): ");
			input.setSex(sc.next());		//문자열 입력받아서 input객체를 이용해 setSex 메소드 호출
			System.out.println("=========================\n");
			
			userMap.put(userId, input);		//입력된 userID를 key, UserDatabase를 객체로 HashMap에 입력
		} else {
			System.out.println("You already have Login ID");		//key값이 없는경우 가입한 이력이 없으므로 해당 내용 출력
			System.out.println("=========================\n");
		}
		
		return true;		//메소드 완료 후 true 값 리턴
	}
	
	public boolean setInfo(String userId) {	//setInfo 메소드 overloading 매개변수 userId
		
		UserDatabase input = new UserDatabase();	//위 메소드와 동일하지만 HashMap의 key값을 이용해 Id 이외의 내용을 수정하여 저장 
		
		System.out.println();
		System.out.println("======== Sign up ========");
		input.setUserId(userId);
		System.out.print("Password: ");
		input.setPassword(sc.next());
		System.out.print("Name: ");
		input.setName(sc.next());
		System.out.print("Birthday(yymmdd): ");
		input.setBirthDate(sc.next());	//문자열 입력받아서 input객체를 이용해 setBirthDate 메소드 호출
		System.out.print("Sex(M/F): ");
		input.setSex(sc.next());		//문자열 입력받아서 input객체를 이용해 setSex 메소드 호출
		System.out.println("=======================\n");
		
		userMap.put(userId, input);		//입력된 userID를 key, UserDatabase를 객체로 hashmap에 입력
		
		return true;		//메소드 완료 후 true 값 리턴
	}
	
	public boolean userLogIn() {	//userLogIn 메소드 시작
		String userId;				//문자열 변수 userId 선언
		String password;			//문자열 변수 password 선언
		
		System.out.println();
		System.out.println("========= Sign in ==========");
		System.out.print("ID: ");
		userId = sc.next();			//userId에 문자열을 입력받아 대입
		System.out.print("Password: ");
		password = sc.next();		//password에 문자열을 입력받아 대입
		System.out.println("=============================");
		
		UserDatabase value = userMap.get(userId);	//UserDatabase 형 value 변수에 userId key 값에 해당하는 내용 객체 저장 
		
        if(userMap.containsKey(userId) && userMap.get(userId).getPassword().equals(password)) {	//입력된 userId와 password 가 모두 일치하면 아래 실행
    		System.out.println("\n======== Information ========");
            System.out.println("name: "+value.getName());			//value에 저장된 userMap HashMap 내용 중 getName 메소드를 이용해 이름 값 호출
            System.out.println("Birthday: "+value.getBirthDate());	//value에 저장된 userMap HashMap 내용 중 gtBirthDate 메소드를 이용해 생년월일 호출
            System.out.println("sex: "+value.getSex());				//value에 저장된 userMap HashMap 내용 중 getSex 메소드를 이용해 성별 값 호출
    		System.out.println("=============================\n");
        } else if(userMap.containsKey(userId)) {			//userId 값만 맞을 경우
        	System.out.println("Wrong password\n");			//패스워드가 틀렸다고 출력
        } else {											//둘다 틀린경우
        	System.out.println("Either ID or password is wrong\n");		//둘다 틀렸다고 출력
        }

	    return true;	//메소드 실행 완료 후 true값 리턴
	}
	
	public boolean changeUser() {		//정보 수정 메소드 시작
		String userId;					//문자열 변수 userId 선언
		
		System.out.println();
		System.out.println("====== Input your ID ======");
		System.out.print("ID: ");
		userId = sc.next();				//userId 변수에 문자열을 입력 받아서 저장
		
		if(userMap.containsKey(userId))	//userId 값이 userMap HashMap에 key로 등록 되어있으면 (true) 
			setInfo(userId);			//setInfo 메소드 호출 매개변수로 userId 가져감
		else
			System.out.println("You have no Login ID");		//ket값이 없는경우 가입한 이력이 없으므로 해당 내용 출력
		
		return true;		//실행 완료후 true 값 리턴
	}
	
	public boolean deleteUser() {		//정보 삭제 메소드 시작
		String userId;					//문자열 변수 userId 선언
		
		System.out.println();
		System.out.println("=== Input your ID to remove ===");
		System.out.print("ID: ");
		userId = sc.next();				//userId에 문자열 입력받아서 저장
		System.out.println("=============================\n");
		
		if(userMap.containsKey(userId))	//userId 값이 userMap HashMap에 key로 등록 되어있으면 (true)
			userMap.remove(userId);		//해당 키값을 가진 정보를 삭제
		else							//key에 등록 되어있지 않으면
			System.out.println("You have no ID to remove");	//아이디 없다고 출력
			
		return true;	//실행 완료 후 true 값 리턴
			
	}
}