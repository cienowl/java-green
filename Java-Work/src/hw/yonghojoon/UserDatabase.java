package hw.yonghojoon;

class UserDatabase {		//UserDatabase 클래스 시작

	private String sex;		//문자열 변수 sex 선언
	private String birthDate;	//문자열 변수 birthDate 선언
	private String name;		//문자열 변수 name 선언
	private String password;	//문자열 변수 password 선언
	private String userId;		//문자열 변수 userId 선언

	public String getSex() {	//getSex 메소드
		return sex;				//sex 값 리턴
	}

	public void setSex(String sex) {	//setSex 메소드 매개변수 sex
		this.sex = sex;			//현재 클래스의 sex에 매개변수로 받은 sex 값을 대임 (this.sex)
	}

	public String getBirthDate() {		//getBirthDate 메소드
		return birthDate;		//birthDate 값 리턴
	}

	public void setBirthDate(String birthDate) {	//setBirthDate 메소드 매개변수 birthDate
		this.birthDate = birthDate;		//현재 클래스의 birthDate에 매개변수로 받은 birthDate 값을 대입 (this.birthDate)
	}

	public String getName() {		//getNme 메소드
		return name;				//name 값 리턴
	}

	public void setName(String name) {	//setName 메소드 매개변수 name
		this.name = name;			//현재 클래스의 name에 매개변수로 받은 name 값을 대입 (this.name)
	}

	public String getPassword() {	//getPassword 메소드
		return password;			//password 값 리턴
	}

	public void setPassword(String password) {	//setPassword 메소드 매개변수 password
		this.password = password;	//현재 클래스의 password에 매개변수로 받은 password 값을 대입 (this.password)
	}

	public String getUserId() {		//getUserId 메소드
		return userId;				//userId 값 리턴
	}

	public void setUserId(String userId) {	//setUserId 메소드 매개변수 userId
		this.userId = userId;		//현재 클래스의 userId에 매개변수로 받은 userId 값을 대입 (this.userId)
	}

}