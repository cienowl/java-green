package d20200428;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FileInOutEx2 {

	public static void main(String[] args) {

		FileInputStream f = null;

		try {
			f = new FileInputStream("d:/etc/write.txt");

			System.out.println("available = " + f.available());	//읽어올 수 있는 바이트 사이즈
			byte b[] = new byte[f.available()];		//읽어올 파일의 크기만큼 생성

			while (f.read(b) != -1) {	//파일 읽어오기 read, 파일의 끝에서 -1이 리턴됨.
			}

			String data = new String(b);	//바이트 데이터 b를 문자열로 변환하여 data에 저장
			System.out.println(data);
		} catch (FileNotFoundException e) {
			System.out.println(e + " => 파일 생성 실패");
		} catch (IOException e) {
			System.out.println(e + " => 파일 읽기 실패");
		} finally {
			try {
				f.close();
			} catch (Exception e) {
				System.out.println(e + " => 닫기 실패");
			}
		}

	}

}
