package d20200428;

import java.io.File;
import java.util.Date;

public class FileInOutLearn {

	public static void main(String[] args) throws Exception {
		File f = new File("d:/etc/a.txt");

		if (f.exists()) {
			System.out.println("존재합니다.");

			if (f.isDirectory()) {
				System.out.println("***** 폴더 정보 *****");
			} else {
				System.out.println("***** 파일 정보 *****");
				System.out.println("파일 이름 : " + f.getName());
				System.out.println("절대 경로 : " + f.getAbsolutePath());
				System.out.println("읽기 기능 : " + f.canRead());
				System.out.println("쓰기 기능 : " + f.canWrite());
				System.out.println("파일 용량 : " + f.length() + "byte");

				long time = f.lastModified();
				Date lastModifier = new Date(time);
				System.out.println("수정 날짜 : " + lastModifier);
				f.delete();
			}
		}
		
	}

}
