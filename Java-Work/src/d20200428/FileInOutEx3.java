package d20200428;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class FileInOutEx3 {

	public static void main(String[] args) {

		FileOutputStream bw = null;		//bw 객체 선언
		Scanner sc = new Scanner(System.in);	//스캐너 객체 생성

		try {
			bw = new FileOutputStream("d:/etc/input.txt");	//bw 객체 생성  

			String data = null;		//문자열 data 변수 초기화
			System.out.println("입력 : ");

			while (!(data = sc.nextLine()).equals("quit")) {	//quit이 입력되면 반복문 탈출
				byte[] b = data.getBytes();		//입력되는 문자열 data를 byte 형으로 변환하여 변수 b에 저장
				bw.write(b);					//bw 객체 파일에 b 내용 쓰기
				System.out.println("입력 내용 : " + data);		//콘솔에 입력 내용 출력
				System.out.println("입력 : ");
			}
		} catch (Exception e) {
			System.out.println(e + " => 오류");
		} finally {
			try {
				bw.close();
			} catch (IOException e) {
				System.out.println(e + "+ close fail");
			}
		}
		sc.close();
		
	}

}
