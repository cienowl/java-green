package d20200428;

import java.io.FileOutputStream;

public class FileInOutEx1 {

	public static void main(String[] args) {
		
		FileOutputStream f = null;	//FileOutputStream 객체 f 생성
		
		try {		//반드시 try를 써야함
			f = new FileOutputStream("d:/etc/temp.txt", true);	//파일을 쓸수있는 권한 가져옴
			f.write(97);		//ASCII 코드 97 = a 가 입력. text파일 내용은 숫자로 인식하지 않음.
			
			String data = "우하하하하 바보";
			byte b[] = data.getBytes();		//문자열을 byte로 변환하여 바이트형 변수 배열 b에 저장
			f.write(b);			//바이트 b 정보를 파일에 쓰기
		} catch(Exception e) {
			System.out.println(e + " => 파일쓰기 실패");
		} finally {
			try {
				f.close();		//권한을 닫음
			} catch(Exception e) {
				System.out.println(e + " => close 실패");
			}
		}

	}

}
