package d20200506;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class LottoGen implements ActionListener {
	String[] buttonName = {"1","2","3","4","5","6"};	//버튼에 사용할 숫자를 문자열 배열로 선언후 초기화
	JButton genNum = new JButton("Generate Num");		//genNum 버튼 객체 생성
	JButton[] buttonNum = new JButton[6];	//
	JTextField textField = new JTextField();
	JPanel panelN = new JPanel();			//panelN 패널 객체 생성
	JPanel panelS = new JPanel();
	JFrame frame = new JFrame();

	String[] rndNumString = new String[6];
	
	public LottoGen() {		//생성자 시작
	
		genNum.setBackground(Color.DARK_GRAY);				//Background 색상 DARK_GRAY로 설정
		genNum.setForeground(Color.YELLOW);					//글자색을 노랑으로 변경
		genNum.setFont(new Font("D2Coding", Font.BOLD, 20));	//D2Coding 폰트 설정, 볼드처리, 글자색 20

		panelN.setLayout(new GridLayout(2,3));
		for(int i = 0; i < buttonName.length; i++) {
			buttonNum[i] = new JButton(buttonName[i]);
			buttonNum[i].setForeground(Color.WHITE);
			buttonNum[i].setBackground(Color.BLACK);
			buttonNum[i].setFont(new Font("D2Coding", Font.BOLD, 50));
			panelN.add(buttonNum[i]);
		}
		
		textField.setBackground(Color.YELLOW);
		
		panelS.setLayout(new GridLayout(1,2));		//panelS 객체에 그리드 (1,2) 레이아웃
		panelS.add(textField);				//레이아웃에 TextField 추가
		panelS.add(genNum);					//레이아웃에 genNum 버튼 추가
	
		frame.setLayout(new BorderLayout());		//전체 레이아웃
		frame.add(panelN, BorderLayout.CENTER);		//Center 위치에 panelN 객체 
		frame.add(panelS, BorderLayout.SOUTH);		//South 위치에 panelS 객체
		
		frame.setSize(500, 300);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		
		genNum.addActionListener(this);
		
	}
	
	public void dispGenNum() {
		String rndNumString = "null";

		for(int i = 0; i < buttonName.length; i++) {
			buttonName[i] = Integer.toString((int) (Math.random() * 45) + 1);
			
			for(int j = 0; j <= i; j++) {
				if(buttonNum[j].getText().equals(buttonName[i])) {
					i--;
				} else {
					buttonNum[i].setText(buttonName[j]);	
					rndNumString += "[" + buttonName[j] + "]";
					System.out.println(rndNumString);
				}

			}
			
		}
		
		textField.setText(rndNumString);
		
	}

	public static void main(String[] args) {
		
		new LottoGen();

	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		dispGenNum();

	}

}
