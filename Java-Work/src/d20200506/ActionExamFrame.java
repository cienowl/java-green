package d20200506;

import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JButton;

public class ActionExamFrame implements WindowListener {

	public ActionExamFrame(){

		Frame frame = new Frame();
		JButton button1 = new JButton("button1");
		JButton button2 = new JButton("button2");
		
		frame.setLayout(new FlowLayout());
		frame.add(button1);
		frame.add(button2);
		
		frame.setSize(500, 300);
		frame.setVisible(true);
//		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);	//Frame 클래스에는 해당 줄의 메서드가 없음. 실행 불가
		frame.addWindowListener(this);		//addWindowListener 메소드 호출
	
		
	}

	public static void main(String[] args) {
		new ActionExamFrame();
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent e) {
		System.exit(0);		//종료
		
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

}
