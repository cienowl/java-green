package d20200506;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class ActionExamJFrame implements ActionListener{
	
	public ActionExamJFrame(){

		JFrame frame = new JFrame();
		JButton button1 = new JButton("button1");
		JButton button2 = new JButton("button2");
		
		frame.setLayout(new FlowLayout());
		frame.add(button1);
		frame.add(button2);
		
		frame.setSize(500, 300);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		
		button1.addActionListener(this);
		button2.addActionListener(this);
		
	}

	public static void main(String[] args) {
		new ActionExamJFrame();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String result = e.getActionCommand();
		
		if(result.equals("button1"))
			System.out.println(result);
		else if(result.equals("button2"))
			System.out.println(result);

	}
	
	

}
