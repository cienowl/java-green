package d20200506;

import java.awt.*;
import javax.swing.*;

public class CalculatorStyleExam {
	final String[] btn_Title = {"+","-","*","/","=","0","1","2","3","4","5","6","7","8","9"};
	
	JTextField txtField = new JTextField("0.0");
	JPanel jp = new JPanel();
	JButton[] jbtn = new JButton[15];
	JFrame f = new JFrame();
	
	public CalculatorStyleExam() {
		f.setLayout(new BorderLayout());
		f.add(txtField, "North");
		f.add(jp,"Center");
		jp.setLayout(new GridLayout(3,5));
		
		for(int i = 0;i<btn_Title.length; i++) {
			
			jp.add(jbtn[i]= new JButton(btn_Title[i]));
			
			if(i<5) {
				jbtn[i].setBackground(new Color(203, 238, 130));
				jbtn[i].setFont(new Font("����", Font.BOLD, 16));
				jbtn[i].setForeground(new Color(223, 133, 16));
			} else if(i<10) {

				jbtn[i].setBackground(new Color(241, 244, 189));
				jbtn[i].setFont(new Font("�ü�ü", Font.BOLD, 20));
				jbtn[i].setForeground(new Color(18, 50, 133));
			} else {
				jbtn[i].setBackground(new Color(203, 238, 130));
				jbtn[i].setFont(new Font("�ü�ü", Font.BOLD, 16));
				jbtn[i].setForeground(new Color(18, 50, 133));
			}
		}
		
		f.setSize(250, 250);
		f.setVisible(true);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void main(String[] args) {
	
		new CalculatorStyleExam();
		
	}

}
