package d20200506;

import java.awt.*;
import javax.swing.*;

public class Main {

	Main() {
		JButton buttonCancel = new JButton("Cancel"); // 버튼 객체 생성
		JButton buttonConfirm = new JButton("Confirm"); // 버튼 객체 생성
		JButton button1 = new JButton("Button1"); // 버튼 객체 생성
		JButton button2 = new JButton("Button2"); // 버튼 객체 생성

		JFrame frame = new JFrame(); // frame 객체 생성
		

//		FlowLayout 메서드 사용
//		frame.setLayout(new FlowLayout());		//FlowLayout() 메서드를 setLayout에 사용
//		frame.add(buttonConfirm);				//버튼 추가
//		frame.add(buttonCancel);	//버튼 추가
//		frame.add(button1);		//버튼 추가
//		frame.add(button2);		//버튼 추가

//		BorderLayout 메서드 사용
//		frame.setLayout(new BorderLayout());
//		frame.add(new Button("NORTH"), BorderLayout.NORTH); // 겹쳐서 사용할시 나중에 호출된 객체가 표시
//	    frame.add(buttonCancel, BorderLayout.SOUTH);
//	    frame.add(button1, BorderLayout.EAST);
//	    frame.add(button2, BorderLayout.WEST);
//		frame.add(buttonConfirm, BorderLayout.CENTER);

//		GridLayout 메서드 사용
//		frame.setLayout(new GridLayout(2,2));
//		frame.add(buttonConfirm);				//버튼 추가
//		frame.add(buttonCancel);	//버튼 추가
//		frame.add(button1);		//버튼 추가
//		frame.add(button2);		//버튼 추가

//		JPanel 클래스 사용
		JPanel panel = new JPanel(); // 컴포넌트
		panel.add(buttonConfirm);
		panel.add(buttonCancel);
		
		frame.setLayout(new BorderLayout());
		frame.add(panel, BorderLayout.EAST);		//패널 컴포넌트 사용하여 삽입
		frame.add(button1, BorderLayout.WEST);
		frame.add(button2, BorderLayout.SOUTH);

		panel.setLayout(new FlowLayout());
		frame.setSize(500, 500); // 프레임 객체 사이즈 조정
		frame.setVisible(true); // 프레임 보여주기
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE); // 기본 최소화, 최대화, 닫기 버튼 추가

	}

	public static void main(String[] args) {

		Main startMain = new Main();

	}

}
