package d20200506;

import java.awt.*;
import javax.swing.*;

public class AwtSwing_InClassEx {

	public AwtSwing_InClassEx() {

		JFrame frame = new JFrame(); // 프레임 객체 생성
		JPanel panelC = new JPanel(); // GridLayout을 쓰기 위해 패널 생성

		panelC.setLayout(new GridLayout(3, 5)); // GridPanel 을 사용한 Layout 지정
		panelC.add(new JButton("+"));
		panelC.add(new JButton("-"));
		panelC.add(new JButton("*"));
		panelC.add(new JButton("/"));
		panelC.add(new JButton("="));
		panelC.add(new JButton("0"));
		panelC.add(new JButton("1"));
		panelC.add(new JButton("2"));
		panelC.add(new JButton("3"));
		panelC.add(new JButton("4"));
		panelC.add(new JButton("5"));
		panelC.add(new JButton("6"));
		panelC.add(new JButton("7"));
		panelC.add(new JButton("8"));
		panelC.add(new JButton("9"));

		frame.setLayout(new BorderLayout()); // 프레임을 BorderLayout() 형태 사용
		frame.add(new JTextField(), BorderLayout.NORTH); // text input 필드 North에 배치
		frame.add(panelC, BorderLayout.CENTER); // 그리드 패널을 Center에 배치

		frame.setSize(500, 300); // 프레임 객체 사이즈 조정
		frame.setVisible(true); // 프레임 보여주기
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE); // 기본 최소화, 최대화, 닫기 버튼 추가

	}

	public static void main(String[] args) {

		new AwtSwing_InClassEx();

	}

}
