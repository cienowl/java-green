package d20200602;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DataAccess {
	
	private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	private static final String DB = "test";
	private static final String JDBC_URL = "jdbc:mysql://localhost/"+DB;
	private static final String TABLE = "eventregist";
	private static final String USER = "root";
	private static final String PASSWORD = "34811005";
	private Connection conn = null;
	private Statement stmt = null;
	/*
	+-------+-------------+------+-----+---------+-------+
	| Field | Type        | Null | Key | Default | Extra |
	+-------+-------------+------+-----+---------+-------+
	| name  | varchar(50) | YES  |     | NULL    |       |
	| email | varchar(50) | YES  |     | NULL    |       |
	+-------+-------------+------+-----+---------+-------+
	*/
	public DataAccess(){
		try {
			Class.forName(JDBC_DRIVER);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void connectDatabase(){
		try {
			conn = DriverManager.getConnection(JDBC_URL, USER, PASSWORD);			
			stmt = conn.createStatement();
		} catch(Exception e) {
			e.printStackTrace();
		} 
		
	}
	
	public void disconnectDatabase(){
		if(stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} 
		if(conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void insertDatabase(DataTransfer dto){
		
		String sqlCommand = "insert into " + TABLE + " values('" + dto.getName() + "', '" + dto.getEmail() + "')"; 
		
		connectDatabase();
		
		try {
			stmt.executeUpdate(sqlCommand);
		}  catch(SQLException e) {
			System.out.println("����" + e);
		} finally {
			disconnectDatabase();
		}
	}
	
	public ArrayList<DataTransfer> readDatabase(){
		ArrayList<DataTransfer> registeredMember = new ArrayList<DataTransfer>();
		
		String sqlCommand = "select * from " + TABLE + ";";
		
		ResultSet rs = null;
		
		connectDatabase();
		
		try {
			
			rs = stmt.executeQuery(sqlCommand);
			
			while(rs.next()) {
				DataTransfer dto = new DataTransfer();
				dto.setName(rs.getString(1));
				dto.setEmail(rs.getString(2));
				
				registeredMember.add(dto);
			}
			
		} catch(SQLException e) {
			System.out.println("����" + e);
		} finally {
			disconnectDatabase();
		}
		
		return registeredMember;
	}
}
