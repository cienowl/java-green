package d20200602;

import java.util.ArrayList;
import java.util.Scanner;

public class EventRegist {
		
	public void registEvent(){
		DataAccess dao = new DataAccess();
		DataTransfer dto = new DataTransfer();
		Scanner inputScanner = new Scanner(System.in);
		
		System.out.println("## 이벤트 등록을 위해 이름과 이메일을 입력하세요.");
		System.out.print("이름: ");
		dto.setName(inputScanner.next());
		System.out.print("\n이메일: ");
		dto.setEmail(inputScanner.next());
		System.out.println();
		
		dao.insertDatabase(dto);
		
		inputScanner.close();
	}
	
	public void viewRegistered(){
		DataAccess dao = new DataAccess();
		ArrayList<DataTransfer> list = new ArrayList<DataTransfer>();
		
		list = dao.readDatabase();
		
		System.out.println("# 등록자 명단");
		
		for(int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i).getName() + ", " + list.get(i).getEmail());	
		}
		
	}

	public static void main(String[] args) {
		EventRegist er = new EventRegist();
		er.registEvent();
		er.viewRegistered();
	}

}
