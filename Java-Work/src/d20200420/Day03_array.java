package d20200420;

/*
* Array(배열): 변수 -> 배열 -> 컬렉션
- 변수들의 집합
- 하나의 이름으로 여러개 변수를 만드는 것.
- 자료형[] 배열명 = new 자료형[개수];
- 배열 사용시 반드시 번지표시
- 배열 사용시 반드시 반복문(for) 사용
- 배열명.length : 배열의 갯수
*/

class Day03_array
{
	public static void main(String[] args) 
	{
		int[] f = new int[5];
		f[0] = 100;
		f[1] = 200;

		for(int idx=0; idx<5; idx++){
			System.out.println(f[idx]);
		}
		System.out.println();

		// 사이즈 5인 double형 배열 선언, 값 지정 -> 출력
		// 사이즈 5인 cahr형 배열 선언, 값 지정 -> 출력
		// 사이즈 5인 String형 배열 선언, 값 지정 -> 출력
		double[] silsu = new double[5];
		for(int idx=0; idx<silsu.length; idx++){
			silsu[idx] = Math.random();
		}

		char[] character = new char[5];	
		character[0] = 'A';
		character[1] = 'B';
		character[2] = 'C';
		character[3] = 'D';
		character[4] = 'E';
		
		String[] string = new String[5];
		string[0] = "AA";
		string[1] = "BB";
		string[2] = "CC";
		string[3] = "DD";
		string[4] = "EE";

		for(int idx=0; idx<5; idx++){
			System.out.println(silsu[idx]);
			System.out.println(character[idx]);
			System.out.println(string[idx]);
			System.out.println();
		}

	}
}
