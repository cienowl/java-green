package d20200420;

//Q2. 난수를 이용해 사이즈가 10인 배열의 값을 지정 (값의 범위: 1~10)
//		키보드로 값을 하나 입력 -> 입력받은 값과 동일한 값이 배열안에 존재하는 여부 확인, 값의 위치

import java.util.Scanner;

class Day03_ArrayQ2
{
	public static void main(String[] args) 
	{
		int i, guessNum, flag=0;
		int[] arr = new int[10];		//난수를 저장할 배열
		int[] index = new int[10];		//같은 값이 존재할경우 복사할 배열

		for(i=0; i<arr.length; i++){		//배열 길이 만큼
			arr[i] = (int)(Math.random()*10)+1;	//랜덤 수 생성해서 저장
		}
				
		System.out.print("Input your guess: ");
		Scanner scInt = new Scanner(System.in);	//scInt Scanner 객체 생성
		guessNum = scInt.nextInt();				//입력된 숫자 guessNum에 저장

		for(i=0; i<arr.length; i++){			//배열 길이만큼
			System.out.println("arr[" + i + "]:" + arr[i]);	//내용 출력
			if(guessNum == arr[i]){				//만약 입력된 숫자 guessNum과 배열값이 같으면
				index[i] = arr[i];				//복사할 배열에 내용 복사
			}
		}
		System.out.println();

		for(i=0; i<index.length; i++){	//복사된 index 배열 길이만큼 반복
			if(index[i] != 0) {			//index 배열의 내용이 있을 때만 (null값이 아닐때만)
				System.out.println("Same element index: " + i);	//내용 출력
				flag = 1;				//동일 값이 있는 경우 flag=1 (분기문 출력을 위해)
			}
		}
		
		if(flag == 0)					//동일 값이 없는 경우 flag=0
			System.out.println("No same element exist.");	//동일 값 없다고 출력
	}
}
