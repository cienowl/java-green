package d20200420;

//Q1. 10명의 학생의 점수를 입력받아 총합, 평균을 구하는 프로그램 작성.

import java.util.Scanner;

class Day03_ArrayQ1
{
	public static void main(String[] args) 
	{
		int[] score = new int[10];		//점수 배열길이 10인 변수 생성
		int total=0;					//총점 변수
		double avg;						//평균

		Scanner scInt = new Scanner(System.in);		//scInt Scanner 객체 생성

		System.out.println("===== Scores for 10 students =====");

		for(int i = 0; i<score.length; i++){		//score 배열 길이만큼 반복
			System.out.print("Input score [" + (i+1) + "]: ");	//인원수를 표현하면서
			score[i] = scInt.nextInt();				//score 배열에 점수 저장
			total += score[i];						//저장과 동시에 총합 계산
			
		}
		avg = total/(double)score.length;			//계산된 총합으로 평균 계산
		
		System.out.println();
		System.out.println("Total: " + total + "  Avg: " + avg);	//퐁합과 평균 출력
	}
}
