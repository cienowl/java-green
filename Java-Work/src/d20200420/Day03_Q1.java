package d20200420;

// Q1. 1부터 n까지의 합계
import java.util.Scanner;

class Day03_Q1 {
	public static void main(String[] args) {
		int n, tot=0;		//합계를 위한 tot, 유저 인풋 변수 n 선언
		
		Scanner scNum = new Scanner(System.in);	//scNum Scanner 객체 생성

		System.out.print("Input num: ");
		n = scNum.nextInt();			//변수 n에 사용자 정의 숫자 저장
		scNum.close();					//scNum Scanner 객체 종료

		for(int i=1; i<=n; i++){		//1부터 입력된 숫자까지 반복
			tot += i;					//합계에 누적하여 저장
		}

		System.out.println("Sum to n: " + tot);	//총점 출력
	}
}
