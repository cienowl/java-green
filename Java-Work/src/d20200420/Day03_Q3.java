package d20200420;

// Q3. 난수 추출(0~19) -> 난수를 맞추는 프로그램 프로그램

import java.util.Scanner;

class Day03_Q3 {
	public static void main(String[] args) {
	
		int step=1, userNum=0, goalNum=(int)(Math.random()*20);	//변수 선언, goalNum 에는 0~19까지의 정수형 난수 저장
		
		System.out.print("intput your number: ");
		Scanner scInt = new Scanner(System.in);		//scInt Scanner 객체 생성
		
		while(goalNum != userNum) {		//goalNum과 입력 받을 userNum이 다를 동안 반복
			userNum = scInt.nextInt();	//사용자 정의 숫자 입력

			if(userNum > goalNum) {		//사용자 정의 숫자가 goalNum 보다 크면
				System.out.println("Your number is higher. " + step + " times"); //입력 받은 숫자가 크다고 출력
			} else if(userNum < goalNum) {	//사용자 정의가 goalNum 보다 작으면
				System.out.println("Your number is lesser. " + step + " times");	//입력 받은 숫자가 작다고 출력
			} else {					//숫자가 동일할 경우 
				System.out.println("Right answer! " + step + " times");	//정답을 맞췄다고 횟수와 함께 출력
			}

			step++;
		}
		scInt.close();	//scInt Scanner 객체 종료
	}
}
