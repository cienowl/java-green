package d20200420;

// Q2. 정수 n개만큼 입력 받음. 입력받은 정수들의 함계, 단!! 음수값은 합계에서 제외!!
import java.util.Scanner;

class Day03_Q2 {
	public static void main(String[] args) {
		int n, element, tot=0;				//변수 선언 입력받는 수 개수, element에 각 입렵받은 숫자 저장, tot 총점
		
		Scanner scInt = new Scanner(System.in);	//scInt Scanner 객체 생성

		System.out.print("How many numbers? ");
		n = scInt.nextInt();					//변수 n에 scInt 객채를 사용하여 사용자 숫자 저장
		
		for(int i=1; i<=n; i++){			// 입력받은 n개의 숫자를 입력 받기위해 반복
			System.out.print("Input number: ");
			element = scInt.nextInt();		//element 변수에 입력 받은 정수 저장

			if(element>=0) {				//element 값이 양수이면,
				tot += element;				//총점 tot 변수에 누적 합
			} 
		}
		scInt.close();						//scInt 객체 종료

		System.out.println("Total of positive input numbers = " + tot);	//총점 출력
	}
}
