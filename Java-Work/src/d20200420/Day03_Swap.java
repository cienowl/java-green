package d20200420;

class Day03_Swap
{
	public static void main(String[] args) 
	{
		int temp, k;
		int[] arr = {10,50,30,40,22};

		System.out.println("===== 실행 전 값 =====");

		for(k=0; k<5; k++){
			System.out.println(arr[k] + " ");
		}

		for(int j = 0; j<4; j++) {
			for(int i = j; i<5; i++) {
				if(arr[j] > arr[i]) {
					temp = arr[j];
					arr[j] = arr[i];
					arr[i] = temp;
				}
			}
		}
		System.out.println("===== 실행 후 값 =====");

		for(k = 0; k<5; k++) {
			System.out.println(arr[k] + " ");
		}
	}
}
