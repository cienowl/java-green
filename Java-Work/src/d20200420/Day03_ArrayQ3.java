package d20200420;

//Q3. 난수를 이용해 사이즈가 5인 배열의 값을 지정(값의 범위: 1~10) 지정한 값들은 중복되지 않는다.

class Day03_ArrayQ3
{
	public static void main(String[] args) 
	{
		int[] arr = new int[5];
		int idx, j, randNum;

		for(idx=0; idx<arr.length; idx++) {		//선언된 배열 길이만큼 반복
			randNum = (int)(Math.random()*10)+1;	//1~10 까지 난수 생성 후 randNum 에 기억
			
			if(idx==0)						//i=0 일때만 작동, 배열 0번 인덱스 초기화용
				arr[idx] = randNum;			//첫 난수는 무조건 0번 인덱스 저장
			
			for(j=0; j<idx; j++) {			//앞선 배열부터 새로운 난수를 저장할 위치까지
				if(arr[j] != randNum) {		//앞선 모든 배열값과 난수 값이 다르면
					arr[idx] = randNum;		//현재 배열 주소에 난수 입력
				} else {					//앞선 모든 배열값과 한개라도 같으면
					idx--;					//같은 위치(i 증가 x)에서 1번 더돌림
					break;					//난수를 저장하지 않고 반복문 탈출
				}
			}
		}
	
/*	
		int[] arr = new int[5];
		int y = 0;
		for(int x = 0; x<num.length; x++) {
			while(y < x) {
				y = 0;
				arr[x] = (int)(Math.random()*10)+1;
				for(y=0; y<x; y++) {
					if(arr[x] == arr[y]) break;
				}
			}
		}
*/

		for(idx=0; idx<arr.length; idx++) {		//배열 길이 만큼 반복
			System.out.print(arr[idx] + " ");	//배열 내용 출력
		}
	}
}
