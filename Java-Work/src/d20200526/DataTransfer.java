package d20200526;

class DataTransfer {
	
	private String itemLot;
	private String itemName;
	private int itemPrice;
	private String itemManufacturer;
	
	public DataTransfer() {
		this.itemLot = "default";
	}
	public String getItemLot() {
		return itemLot;
	}
	public void setItemLot(String itemLot) {
		this.itemLot = itemLot;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public int getItemPrice() {
		return itemPrice;
	}
	public void setItemPrice(int itemPrice) {
		this.itemPrice = itemPrice;
	}
	public String getItemManufacturer() {
		return itemManufacturer;
	}
	public void setItemManufacturer(String itemManufacturer) {
		this.itemManufacturer = itemManufacturer;
	}
	
}
