package d20200526;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

class WindowMain implements ActionListener {
	
	JFrame frame = new JFrame("Product Manager Application v0.0.2");
	JButton addButton, findButton, deleteButton;
	JTextField resultCommField, itemNameField, itemPriceField, itemManufacturerField;
	JTextArea outputArea;
	JLabel itemLotLabel, itemNameLabel, itemPriceLabel, itemManufacturerLabel;
	JScrollPane scrollPanel;
	
	JComboBox<String> comboBox;
			
	void mainMenu() {
		
		outputArea = new JTextArea();
		scrollPanel = new JScrollPane(outputArea);
		
		outputArea.setEditable(false);
		
		comboBox = new JComboBox<String>();
				
		addButton = new JButton("등록");
		findButton = new JButton("조회");
		deleteButton = new JButton("삭제");
		
		itemLotLabel = new JLabel("관리번호");
		itemNameLabel = new JLabel("상품명");
		itemPriceLabel = new JLabel("단가");
		itemManufacturerLabel = new JLabel("제조사");
		
		resultCommField = new JTextField("프로그램 시작");
		resultCommField.setEditable(false);
		
		itemNameField = new JTextField();
		itemPriceField = new JTextField();
		itemManufacturerField = new JTextField();
		
		JPanel westPanel = new JPanel();
		westPanel.setPreferredSize(new Dimension(250,200));
		westPanel.setLayout(new GridLayout(4,2));
		westPanel.add(itemLotLabel);
		westPanel.add(comboBox);
		westPanel.add(itemNameLabel);
		westPanel.add(itemNameField);
		westPanel.add(itemPriceLabel);
		westPanel.add(itemPriceField);
		westPanel.add(itemManufacturerLabel);
		westPanel.add(itemManufacturerField);
		
		JPanel southPanel = new JPanel();
		southPanel.setLayout(new FlowLayout());
		southPanel.add(addButton);
		southPanel.add(findButton);
		southPanel.add(deleteButton);
		
		frame.setLayout(new BorderLayout());
		frame.add(resultCommField, BorderLayout.NORTH);
		frame.add(westPanel, BorderLayout.WEST);
		frame.add(scrollPanel, BorderLayout.CENTER);
		frame.add(southPanel, BorderLayout.SOUTH);
		
		showItems("*");
		
		addButton.addActionListener(this);
		findButton.addActionListener(this);
		deleteButton.addActionListener(this);
		
		frame.setVisible(true);
		frame.setSize(650,300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	
	public void showItems(String selectedItemLot) {	//showItems 메서드 시작 매개변수 selectedItemLot ("*" 또는 콤보박스에서 선택한 관리번호)

		DataAccess dao = new DataAccess();	//DataAccess 객체 생성 dao
		DataTransfer dto = new DataTransfer();		//DataTransfer 객체 생성 dto
		ArrayList<DataTransfer> initList = new ArrayList<DataTransfer>();	//ArrayList<DataTransfer> 객체 생성 initList
		
		initList = dao.sqlSelectAll();	//DataAccees 클래스의 sqlSelectAll() 메서드 호출 후 initList 에 ArrayList 저장
		
		comboBox.removeAllItems();	//기존 콤보박스 내용 모두 삭제
		comboBox.addItem("선택");	//콤보박스 첫 아이템을 "선택"으로 추가
		
		String resultString = "관리번호\t상품명\t단가\t제조사\n";	//출력문 제일 윗줄 인덱스
		for(int i = 0; i < initList.size(); i++) {					//ArrayList 사이즈 크기만큼 반복
			resultString += initList.get(i).getItemLot() + "\t"	 + initList.get(i).getItemName()  + "\t" + initList.get(i).getItemPrice()  + "\t" + initList.get(i).getItemManufacturer() + "\n";
			comboBox.addItem(initList.get(i).getItemLot());		//ArrayList에서 관리번호 추출하여 콤보박스 항목 추가
		}
				
		outputArea.setText(resultString);	//TextArea 에 resultString 출력
		
		if(selectedItemLot != "*") {		//showItems 호출시 "*"가 아닌 콤보박스에서 선택한 관리번호가 매개변수로 오면 실행
			dto = dao.sqlSelect(selectedItemLot);	//DataAccess 클래스에 있는 sqlSelect 메서드 호출 (매개변수로 관리번호)후, 리턴값을 DataTransfer 객체 dto에 저장 
			
			itemNameField.setText(dto.getItemName());	//상품명 텍스트필드에 리턴된 dto 객체의 ItemName 값을 가져와서 표시
			itemPriceField.setText(Integer.toString(dto.getItemPrice()));	//단가 텍스트필드에 리턴된 dto 객체의 ItemPrice 값을 가져와서 표시
			itemManufacturerField.setText(dto.getItemManufacturer());	//제조사 텍스트필드에 리턴된 dto 객체의 ItemManufacturer 값을 가져와서 표시
		}
		
	}
	
	public void resetTextField() {	//resetTextField 메서드. 호출시 아래 텍스트필드의 표시값을 null으로 세팅 (내용지움)
		itemNameField.setText(null);
		itemPriceField.setText(null);
		itemManufacturerField.setText(null);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		DataTransfer dto = new DataTransfer();	//DataTransfer 객체 setInfo 생성
		DataAccess dao = new DataAccess();	//DataAccess 객체 dao 생성
		
		if(e.getSource() == addButton) {	//등록 버튼 클릭시 실행
			
			dto.setItemName(itemNameField.getText());	//DataTransfer 객체 setInfo에 itemName을 상품명 텍스트필드 값으로 저장
			dto.setItemPrice(Integer.parseInt(itemPriceField.getText()));	//DataTransfer 객체 setInfo에 itemPrice를 단가 텍스트필드 값으로 저장
			dto.setItemManufacturer(itemManufacturerField.getText());	//DataTransfer 객체 setInfo에 itemManufacturer를 제조사 텍스트필드 값으로 저장
			dao.sqlAdd(dto);	//DataAccess 클래스의 sqlAdd 메서드 호출. (setInfo 매개변수) 
			
			resetTextField();	//입력한 텍스트필드 리셋
			showItems("*");	//DB 내용 등록 후 showItems 메서드 호출
					
			resultCommField.setText("결과: 입력값이 추가되었습니다.");	//결과 커맨드 창에 해당 내용 출력
			
		} else if(e.getSource() == findButton) {	//조회 버튼 클릭시 실행
			
			resetTextField();	//텍스트 필드에 출력하기 전에 텍스트창 리셋
			String selectedItemLot = (String) comboBox.getSelectedItem();	//selectedItemLot 문자열 변수에 콤보박스에서 선택한 관리번호를 저장
			
			showItems(selectedItemLot);	//콤보박스에서 선택한 관리번호를 매개변수로 showItems 호출

			resultCommField.setText("결과: 선택한 관리번호: " + selectedItemLot + "의 상품을 가져왔습니다.");	//결과 커맨드 창에 해당 내용 출력
			
		} else if(e.getSource() == deleteButton) {	//삭제 버튼 클릭시 실행
			
			resetTextField();	//텍스트창 리셋
			String selectedItemLot = (String) comboBox.getSelectedItem();	//selectedItemLot 문자열 변수에 콤보박스에서 선택한 관리번호를 저장
			
			dao.sqlDelete(selectedItemLot);	//DataAccess 클래스의 sqlDelete 메서드 호출. 매개변수는 콤보박스에서 선택한 관리번호
			
			resultCommField.setText("결과: 선택한 관리번호: " + selectedItemLot + "의 상품이 삭제되었습니다.");	//결과 커맨드 창에 해당 내용 츨력
			
			showItems("*");	//화면에 DB에서 삭제된 항목을 제거하기 위해 재출력
			
		}
		
	}

}
