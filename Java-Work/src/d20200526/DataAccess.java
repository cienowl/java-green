package d20200526;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DataAccess {
	/* Database name = test, table name = item
	+-------------------+-------------+------+-----+---------+----------------+
	| Field             | Type        | Null | Key | Default | Extra          |
	+-------------------+-------------+------+-----+---------+----------------+
	| item_lotnumber    | int(11)     | NO   | PRI | NULL    | auto_increment |
	| item_name         | varchar(45) | YES  |     | NULL    |                |
	| item_price        | int(11)     | YES  |     | NULL    |                |
	| item_manufacturer | varchar(50) | YES  |     | NULL    |                |
	+-------------------+-------------+------+-----+---------+----------------+
	 */
	
	private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	private static final String DB = "test";
	private static final String JDBC_URL = "jdbc:mysql://localhost/"+DB;
	private static final String TABLE = "item";
	private static final String USER = "root";
	private static final String PASSWORD = "34811005";
	private Connection conn = null;
	private Statement stmt = null;
	
	public DataAccess() {
		try {
			Class.forName(JDBC_DRIVER);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void connectSql() {
		
		try {
			conn = DriverManager.getConnection(JDBC_URL, USER, PASSWORD);			
			stmt = conn.createStatement();
		} catch(Exception e) {
			e.printStackTrace();
		} 
		
	}
	
	public void disconnSql() {

		if(stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} 
		if(conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
		
	public void sqlAdd(DataTransfer inputInfo) {	//dto 객체변수 inputInfo를 매개변수로 받아서 실행
		
		String sqlCommand = "insert into " + TABLE + " values(" + inputInfo.getItemLot() + ", '" + inputInfo.getItemName() + "', " + inputInfo.getItemPrice() + ", '" + inputInfo.getItemManufacturer() + "')"; 
		
		connectSql();
		
		try {
			stmt.executeUpdate(sqlCommand);
		}  catch(SQLException e) {
			System.out.println("에러" + e);
		} finally {
			disconnSql();
		}
		
	}
	
	public void sqlDelete(String selectedItemLot) {	//콤보박스에서 선택된 관리번호를 selectedItemLot 로 받아서 실행
		
		String sqlCommand = "delete from " + TABLE + " where item_lotnumber = '" + selectedItemLot + "';";
		
		connectSql();
		
		try {
			
			stmt.executeUpdate(sqlCommand);
			
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			disconnSql();
		}
	}
	
	public DataTransfer sqlSelect(String selectedItemLot) {	//콤보박스에서 선택된 관리번호를 selectedItemLot 로 받아서 실행 후 DataTransfer 객체 dto 리턴
		
		DataTransfer dto = new DataTransfer();
		String sqlCommand = "select * from " + TABLE + " where item_lotnumber = '" + selectedItemLot + "'";
		
		ResultSet rs = null;
		
		connectSql();
		
		try {
			
			rs = stmt.executeQuery(sqlCommand);
			
			while(rs.next()) {
				dto.setItemLot(rs.getString(1));
				dto.setItemName(rs.getString(2));
				dto.setItemPrice(rs.getInt(3));
				dto.setItemManufacturer(rs.getString(4));
			}
			
		} catch(SQLException e) {
			System.out.println("에러" + e);
		} finally {
			disconnSql();
		}
		
		return dto;
	}
	
	public ArrayList<DataTransfer> sqlSelectAll() {	//db 내용 전체 읽고 ArrayList에 담아서 리턴 (index, DataTransfer)
		ArrayList<DataTransfer> dataList = new ArrayList<DataTransfer>();
		
		String sqlCommand = "select * from " + TABLE;

		ResultSet rs = null;
		
		connectSql();
		
		try {
			rs = stmt.executeQuery(sqlCommand);
			
			int index=0;
			while(rs.next()) {
				DataTransfer dto = new DataTransfer();
				dto.setItemLot(rs.getString(1));
				dto.setItemName(rs.getString(2));
				dto.setItemPrice(rs.getInt(3));
				dto.setItemManufacturer(rs.getString(4));
				
				dataList.add(index, dto);
				index++;
			}
			
		} catch(SQLException e) {
			System.out.println("에러" + e);
		} finally {
			disconnSql();
		}
		
		return dataList;
	}
	
}
