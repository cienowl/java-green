/*
 * 상품명: 입력
 * 단가: 입력
 * 제조사: 입력
 * 
 * 
 * 등록버튼을 누르면 실제 DB에 반영하고 출력
 * 또 등록을 하면 DB에 추가하고 출력
 * 
 * 관리번호 셀렉트 번호에도 추가된 관리번호 목록 추가
 * 
 * 셀렉트번호에서 고른 후 조회버튼을 누르면 
 * 상품명, 단ㄱ, 제조사 텍스트 필드에 출력
 * 삭제를 누르면 셀렉트 항목도 빠지고 출력에도 빠짐
 * 
 * 화면도 있음
 */

package d20200526;

public class MainClass {

	public static void main(String[] args) {
		
		new WindowMain().mainMenu();	//WindowMain 클래스의 mainMenu() 메서드 실행
		
	}

}
