package d20200507;

import java.awt.Frame;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class PopMenuExam extends Frame {
	
	PopupMenu pm = new PopupMenu();		//팝업메뉴 pm 객체 생성
	MenuItem pm_item1 = new MenuItem("전체선택");	//메뉴아이템 pm_item1 객체 생성
	MenuItem pm_item2 = new MenuItem("복사하기");	//메뉴아이템 pm_item2 객체 생성
	MenuItem pm_item3 = new MenuItem("잘라내기");	//메뉴아이템 pm_item3 객체 생성
	MenuItem pm_item4 = new MenuItem("붙여넣기");	//메뉴아이템 pm_item4 객체 생성
	
	public PopMenuExam() {		//생성자
		super("팝업메뉴");		//Frame 생성자에 "팝업메뉴" 매개변수로 실행
		pm.add(pm_item1);		//팝업메뉴 객체에 메뉴아이템 pm_item1 객체 추가
		pm.addSeparator();		//팝업메뉴에 세퍼레이터 추가
		pm.add(pm_item2);		//팝업메뉴 객체에 메뉴아이템 pm_item2 객체 추가
		pm.add(pm_item3);		//팝업메뉴 객체에 메뉴아이템 pm_item3 객체 추가
		pm.add(pm_item4);		//팝업메뉴 객체에 메뉴아이템 pm_item4 객체 추가
		add(pm);	//Frame 에 팝업메뉴 pm 객체 추가
		
		setSize(200,400);		//프레임 사이즈 지정
		setVisible(true);		//프레임 보이게 함
		
		this.addWindowListener(new WindowAdapter() {	
			@Override
			public void windowClosing(WindowEvent e) {
				System.exit(0);		//정상종료
			}
		});
		
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent me) {
				if(me.getButton() == MouseEvent.BUTTON3)		//BUTTON3 우클릭
					pm.show(PopMenuExam.this, me.getX(), me.getY());
			}
		});
	}
	
	public static void main(String[] args) {
		
		new PopMenuExam();		//PopMenuExam() 클래스 생성

	}

}
