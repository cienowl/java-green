package d20200507;

import java.awt.Button;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

public class ActionEventExam extends JFrame {		//JFrame 상속
	Button b1, b2, b3;		//버튼클래스 선언
	
	public ActionEventExam() {
		setLayout(new FlowLayout());
		
		ActionListener wa = new ActionListener() {		//아래 메소드들만 wa에 가져다 놓음
			public void actionPerformed(ActionEvent e) {
				if(e.getActionCommand().equals("Enable")) {
					b1.setEnabled(false);
					b2.setEnabled(true);
					b3.setForeground(Color.BLACK);
				} else if(e.getActionCommand().equals("Disable")) {
					b1.setEnabled(true);
					b2.setEnabled(false);
					b3.setForeground(Color.BLUE);
				}
			}
		};
		
		b1 = new Button("Enable");
		b2 = new Button("Disable");
		b3 = new Button("Tester");
		
		b2.setEnabled(false);
		b1.addActionListener(wa);
		b2.addActionListener(wa);
		
		add(b1);
		add(b2);
		add(b3);
		
	}

	public static void main(String[] args) {
		
		ActionEventExam f = new ActionEventExam();
		f.setVisible(true);
		f.setSize(200,300);
		f.setDefaultCloseOperation(EXIT_ON_CLOSE);

	}

}
