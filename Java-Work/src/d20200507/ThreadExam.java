package d20200507;

public class ThreadExam extends Thread {

	public ThreadExam(String name) {
		super(name);		//Thread 생성자 호출 매개변수 name
	}

	@Override
	public void run() {		//Thread 클래스의 run() 메서드 실행 (오버라이드)
		for (int i = 1; i <= 5; i++) {
			System.out.println(getName() + "=>" + i + ", Priority=" + getPriority());
			System.out.println(i);
		}
		System.out.println(getName() + "=>End");
	}

	public static void main(String[] args) {
		System.out.println("============== Thread Start =============");
		ThreadExam th = new ThreadExam("1st Thread");
		ThreadExam th2 = new ThreadExam("2nd Thread");

		th.start();
		th2.start();	//th 스레드가 실행되는 동안 th2 스레드 시작
		System.out.println("=============== main 완료 ===============");

	}

}
