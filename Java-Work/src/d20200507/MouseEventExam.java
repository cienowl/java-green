package d20200507;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class MouseEventExam extends Frame implements MouseListener, MouseMotionListener {
	JLabel label = new JLabel("마우스를 드래그");
	JTextField text = new JTextField();

	public MouseEventExam() {
		super("마우스");
		add(label, "North");
		add(text, "South");

		setBackground(Color.pink);

		setSize(500, 400);
		setVisible(true);

		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		this.addWindowListener(new WinEvent());
	}

	public static void main(String[] args) {		//main 메소드
		new MouseEventExam();
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	

}

//메인 클래스에서 상속을 더이상 받을 수 없는 경우, 다른 클래스를 만들어서 상속 받아 객체 생성하고 사용 가능.
//상속을 받는 경우 interface와 달리 원하는 메소드만 상속 가능
class WinEvent extends WindowAdapter {	
	@Override
	public void windowClosing(WindowEvent e) {
		System.exit(0);
	}
	
}
