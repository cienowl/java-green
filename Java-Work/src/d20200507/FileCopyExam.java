package d20200507;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.swing.JOptionPane;

public class FileCopyExam {

	public static void main(String[] args) {		//메인클래스
		String fileName = JOptionPane.showInputDialog("저장할 파일 이름");		//파일이름 입력받음
		FileInputStream fi = null;		//파일인풋스트림 선언
		FileOutputStream fo = null;		//파일아웃풋스트림 선언
		
		try {
			fi = new FileInputStream("D:/read.txt");			//D:/read.txt 파일 객체 생성
			fo = new FileOutputStream("D:/"+fileName+".txt");	//D:/{fileName}.txt 로 객체 생성
			
			int i = 0;
			while((i=fi.read()) != -1) {	//인풋파일에서 뭐든 읽어서 -1이 되면 반복 종료
				fo.write(i);		//해당 내용 아웃풋파일에 쓰기
			}
			
			System.out.println(fileName + ".txt 파일 Copy 완료");	//출력
			
		} catch(Exception e) {
			System.out.println(e + "=> 오류 발생");
		} finally {
			try {
				fi.close();		//인풋파일 클로즈
				fo.close();		//아웃풋파일 클로즈
			} catch(IOException e) {
				System.out.println(e + "=> 닫기 실패");
			}
		}

	}

}
