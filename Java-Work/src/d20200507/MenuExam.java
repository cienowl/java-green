package d20200507;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MenuExam extends JFrame implements ActionListener {
	
	JMenuBar bar = new JMenuBar();		//프레임당 한개만 가능
	JMenu file = new JMenu("파일(F)");			//메뉴객체 생성 여러개 생성가능
	JMenuItem nFile = new JMenuItem("새파일");	//메뉴아이템 객체 생성 여러개 생성가능
	JMenuItem open = new JMenuItem("열기");	//메뉴아이템 객체 생성 여러개 생성가능
	JMenuItem save = new JMenuItem("저장");	//메뉴아이템 객체 생성 여러개 생성가능
	JMenuItem quit = new JMenuItem("종료");	//메뉴아이템 객체 생성 여러개 생성가능
	
	JTextArea text = new JTextArea();		//textArea 객체 생성
	JScrollPane sp = new JScrollPane(text);	//textArea에 스크롤 추가
	
	public MenuExam() {
		file.add(nFile);		//메뉴객체에 nFile 메뉴아이템 추가
		file.add(open);		//메뉴객체에 open 메뉴아이템 추가
		file.add(save);		//메뉴객체에 save 메뉴아이템 추가
		file.addSeparator();		//메뉴객체에 Separator 메뉴아이템 추가
		file.add(quit);		//메뉴객체에 quit 메뉴아이템 추가
		
		bar.add(file);		//메뉴바에 file 객체 추가
		super.setJMenuBar(bar);			//메뉴바 세팅 super는 생략가능함. 상속상태
		super.add(sp, "Center");
		
		setSize(500,400);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public static void main(String[] args) {
		
		new MenuExam();

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	

}
