package d20200507;

import java.util.Calendar;

import javax.swing.JFrame;

public class ThreadExam1 extends JFrame implements Runnable {
	
	public ThreadExam1() {		//생성자 실행
		setSize(700, 400);		//프레임 사이즈 세팅
		setVisible(true);		//프레임 보이게 세팅
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	@Override
	public void run() {	//Runnable 인스턴스의 run() 매서드 재정의
		while(true) {	//무한루프
			Calendar cal = Calendar.getInstance();
			String now = cal.get(Calendar.YEAR) + "년" + (cal.get(Calendar.MONTH)+1) + "월" + cal.get(Calendar.DATE) + "일" + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND);
			super.setTitle(now);
			
			try {
				Thread.sleep(1000);
			} catch(InterruptedException e) {
				System.out.println(e + "---> sleep fail");
			}
		}

	}
	
	public static void main(String[] args) {
		ThreadExam1 re = new ThreadExam1();
		Thread time = new Thread(re,"time");
		time.start();
	}

}
