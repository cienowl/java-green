package d20200507;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

class WindowView implements ActionListener {
	
	JFrame frame = new JFrame();		//프레임 객체 선언
	JPanel p_Main = new JPanel();		//패널 객체 선언
	
	JButton b_Cal = new JButton("계산");		//계산 버튼 객체 선언
	
	JLabel l_Heading = new JLabel("성적처리 프로그램");	//라벨 객체 선언
	JLabel l_Name = new JLabel("이름:");	//라벨 객체 선언
	JLabel l_Hakbun = new JLabel("학번:");	//라벨 객체 선언
	JLabel l_Kor = new JLabel("국어점수:");	//라벨 객체 선언
	JLabel l_Eng = new JLabel("영어점수:");	//라벨 객체 선언
	JLabel l_Mat = new JLabel("수학점수:");	//라벨 객체 선언
		
	JTextField t_Name = new JTextField();	//텍스트필드 객체 선언
	JTextField t_Hakbun = new JTextField();	//텍스트필드 객체 선언
	JTextField t_Kor = new JTextField();	//텍스트필드 객체 선언
	JTextField t_Eng = new JTextField();	//텍스트필드 객체 선언
	JTextField t_Mat = new JTextField();	//텍스트필드 객체 선언
	JTextField t_Result = new JTextField();	//텍스트필드 객체 선언
	
	
	public WindowView() {	//생성자
		
		p_Main.setLayout(new GridLayout(5,2));	//패널에 GredLayout(5,2) 설정
		p_Main.add(l_Name);		//라벨 객체 삽입
		p_Main.add(t_Name);		//텍스트필드 객체 삽입
		p_Main.add(l_Hakbun);	//라벨 객체 삽입
		p_Main.add(t_Hakbun);	//텍스트필드 객체 삽입
		p_Main.add(l_Kor);		//라벨 객체 삽입
		p_Main.add(t_Kor);		//텍스트필드 객체 삽입
		p_Main.add(l_Eng);		//라벨 객체 삽입
		p_Main.add(t_Eng);		//텍스트필드 객체 삽입
		p_Main.add(l_Mat);		//라벨 객체 삽입
		p_Main.add(t_Mat);		//텍스트필드 객체 삽입
			
		frame.setLayout(new BorderLayout());		//프레임 레이아웃 BoderLayout으로 설정
		frame.add(l_Heading, BorderLayout.NORTH);	//North에 l_Heading 라벨 삽입
		frame.add(p_Main, BorderLayout.CENTER);		//Center에 p_Main 패널 삽입
		frame.add(b_Cal, BorderLayout.EAST);		//East에 계산 버튼 삽입
		frame.add(t_Result, BorderLayout.SOUTH);	//South에 결과출력할 textField 삽입
		
		frame.setSize(300, 200);		//프레임 사이즈 지정
		frame.setVisible(true);			//프레임 보여주기
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		
		b_Cal.addActionListener(this);	//클릭시 실행
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {		//클릭시 실행
		String grade;		//학점 저장할 문자열형 변수 생성
		
		int kor = Integer.parseInt(t_Kor.getText());	//텍스트 필드에서 리턴된 문자열을 정수형으로 변환 후 저장
		int eng = Integer.parseInt(t_Eng.getText());	//텍스트 필드에서 리턴된 문자열을 정수형으로 변환 후 저장
		int mat = Integer.parseInt(t_Mat.getText());	//텍스트 필드에서 리턴된 문자열을 정수형으로 변환 후 저장
		
		int tot = kor + eng + mat;	//총점 계산
		double avg = tot/3.0;		//평균 계산
		
		if(avg >= 90) {		//학점 판정
			grade = "A";
		} else if(avg >= 80) {
			grade = "B";
		} else if(avg >= 70) {
			grade = "C";
		} else if(avg >= 60) {
			grade = "D";
		} else {
			grade = "F";
		}
		
		String result = "[총점: " + Integer.toString(tot) + "] [평균: " + Double.toString((Math.round(avg*100))/100.) + "] [학점: " + grade + "]";	//결과에 문자열 저장
 
		t_Result.setText(result);	//결과 textField에 출력
		
	}
	
}


public class SungjukProgramActionListener {

	public static void main(String[] args) {
		
		new WindowView();	//WindowView 객체 생성

	}
	
}
