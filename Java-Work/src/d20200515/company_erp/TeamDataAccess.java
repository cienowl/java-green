package d20200515.company_erp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/* database yhj_inc, table teams.
+---------------+-------------+------+-----+---------+-------+
| Field         | Type        | Null | Key | Default | Extra |
+---------------+-------------+------+-----+---------+-------+
| team_code     | varchar(20) | YES  |     | NULL    |       |
| team_name     | varchar(20) | YES  |     | NULL    |       |
| team_location | varchar(30) | YES  |     | NULL    |       |
| team_tel      | varchar(20) | YES  |     | NULL    |       |
+---------------+-------------+------+-----+---------+-------+
 */

public class TeamDataAccess {
	
	private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	private static final String DB = "yhj_inc";
	private static final String JDBC_URL = "jdbc:mysql://localhost/"+DB;
	private static final String TABLE = "teams";
	private static final String USER = "root";
	private static final String PASSWD = "sjrnfl12^^";
	private Connection conn = null;
	private Statement stmt = null;
	
	public TeamDataAccess() {
		try {
			Class.forName(JDBC_DRIVER);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void connectSql() {
		
		try {
			conn = DriverManager.getConnection(JDBC_URL, USER, PASSWD);			
			stmt = conn.createStatement();
		} catch(Exception e) {
			e.printStackTrace();
		} 
		
	}
	
	public void disconnSql() {

		if(stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} 
		if(conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public boolean checkTeamExist(String inputTeamName) {
		boolean checker = false;
		String sqlCommand = "select * from " + TABLE + " where team_name = '" + inputTeamName + "'";
	
		connectSql();
		
		try {
			
			ResultSet rs = stmt.executeQuery(sqlCommand);
			
			if(rs.next()) {		//매개변수 inputTeamName이 존재하면 true
				checker = true;
			} else {
				checker = false;
			}
			
		}  catch(SQLException e) {
			System.out.println("에러" + e);
		} finally {
		
			disconnSql();
		}
		
		return checker;
	}
	
	public void insertSql(TeamDataTransfer addTeam) {
		
		connectSql();
		
		String sqlCommand = "insert into " + TABLE + " values('"+addTeam.getTeamCode()+"','"+addTeam.getTeamName()+"','"+addTeam.getTeamLocation()+"','"+addTeam.getTeamTel()+"')";
		
		try {
			
			stmt.executeUpdate(sqlCommand);
				
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			disconnSql();
		}
		
	}
	
	public void deleteSql(String deleteTeam) {
		
		connectSql();
		
		String sqlCommand = "delete from " + TABLE + " where team_name = '" + deleteTeam + "';";
		
		try {
			
			stmt.executeUpdate(sqlCommand);
			
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			disconnSql();
		}
		
	}
	
	public void updateSql(TeamDataTransfer updateTeam) {
		connectSql();
		
		String teamCode = updateTeam.getTeamCode();
		String teamName = updateTeam.getTeamName();
		String teamLocation = updateTeam.getTeamLocation();
		String teamTel = updateTeam.getTeamTel();
		
		String sqlCommand = "update teams set team_code = '" + teamCode + "', team_name = '" + teamName + "', team_location = '" + teamLocation + "', team_tel = '" + teamTel + "' where team_name = '" + teamName + "'";
		
		try {
			
			stmt.executeUpdate(sqlCommand);
			
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			disconnSql();
		}
	}
	
	public ArrayList<TeamDataTransfer> selectAllSql(String targetTeamName) {
		
		ArrayList<TeamDataTransfer> dbList = new ArrayList<TeamDataTransfer>();
		String sqlCommand;
		
		if(targetTeamName == "*") {
			sqlCommand = "select * from " + TABLE;
		} else {
			sqlCommand = "select * from " + TABLE + " where team_name = '" + targetTeamName + "';";
		}
		
		ResultSet rs = null;
		
		connectSql();
		
		try {
			rs = stmt.executeQuery(sqlCommand);
			
			int idx=0;
			while(rs.next()) {
				TeamDataTransfer dto = new TeamDataTransfer();
				dto.setTeamCode(rs.getString(1));
				dto.setTeamName(rs.getString(2));
				dto.setTeamLocation(rs.getString(3));
				dto.setTeamTel(rs.getString(4));
				
				dbList.add(idx, dto);
				idx++;
			}
			
		} catch(SQLException e) {
			System.out.println("에러" + e);
		} finally {
			disconnSql();
		}
		
		return dbList;
	}
	
}
