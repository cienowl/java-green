package d20200515.company_erp;

import java.util.ArrayList;

import d20200515.company_erp.ui_windows.ErrorPopUp;

public class TeamDataController {
	
	public void addTeam(TeamDataTransfer dto) {	//$add$ 시 dto 객체 받아옴
		TeamDataAccess dao = new TeamDataAccess();
		
		if(!dao.checkTeamExist(dto.getTeamName())){	//팀이름이 있는 경우 true값 리턴
			dao.insertSql(dto);
		} else {
			String errorMsg = "중복된 부서명이 있습니다.";
			new ErrorPopUp(errorMsg);
		}
		
	}

	public void deleteTeam(String targetTeamName) {
		
		TeamDataAccess dao = new TeamDataAccess();
		String errorMsg;
		
		if(dao.checkTeamExist(targetTeamName)) {
			new TeamDataAccess().deleteSql(targetTeamName);
			errorMsg = targetTeamName + "팀 삭제되었습니다.";
			new ErrorPopUp(errorMsg);
		} else {
			errorMsg = "삭제할 부서가 없습니다.";
			new ErrorPopUp(errorMsg);
		}
		
	}

	public void updateTeam(TeamDataTransfer setdto) { //toDo = $add$ or delete or update or show
		TeamDataAccess dao = new TeamDataAccess();
		String errorMsg;
		String targetTeamName = setdto.getTeamName();
		
		if(dao.checkTeamExist(targetTeamName)) {
			dao.updateSql(setdto);
			errorMsg = targetTeamName + "팀 수정되었습니다.";
			new ErrorPopUp(errorMsg);
		} else {
			errorMsg = "수정할 부서가 없습니다.";
			new ErrorPopUp(errorMsg);
		}
				
	}
	
	public String showSelectedTeam(String targetTeamName) {
		
		TeamDataAccess dao = new TeamDataAccess();
		
		ArrayList<TeamDataTransfer> teamList = new ArrayList<TeamDataTransfer>();
		String resultText = "====================================================\n"
						+ "코드\t부서명\t지역\t전화번호\n"+
						"====================================================\n";
		
		if(dao.checkTeamExist(targetTeamName)) {
			teamList = dao.selectAllSql(targetTeamName);
			resultText += teamList.get(0).getTeamCode() + "\t" + teamList.get(0).getTeamName() + "\t" + teamList.get(0).getTeamLocation() + "\t" + teamList.get(0).getTeamTel() + "\n";
			resultText += "====================================================\n";
		} else {
			String errorMsg = "출력할 부서가 없습니다.";
			new ErrorPopUp(errorMsg);
		}
		
		return resultText;
	}

	public String showAll() {
		
		TeamDataAccess dao = new TeamDataAccess();
		ArrayList<TeamDataTransfer> teamList = new ArrayList<TeamDataTransfer>();	
		String resultText = "====================================================\n"
						+ "코드\t부서명\t지역\t전화번호\n"+
						"====================================================\n";
		
		teamList = dao.selectAllSql("*");		//팀리스트 가져옴
		
		if(teamList.size()!=0) {
			
			for(int i = 0; i<teamList.size(); i++) {
				resultText += teamList.get(i).getTeamCode() + "\t" + teamList.get(i).getTeamName() + "\t" + teamList.get(i).getTeamLocation() + "\t" + teamList.get(i).getTeamTel() + "\n";
			}
			resultText += "====================================================\n";
			
		} else {
			resultText = " ";
			String errorMsg = "출력할 부서가 없습니다.";
			new ErrorPopUp(errorMsg);
		}
		
		return resultText;
	}
	
}
