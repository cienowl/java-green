package d20200515.company_erp.ui_windows;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import d20200515.company_erp.TeamDataController;

public class MainMenu implements ActionListener{
	JButton addButton, deleteButton, updateButton, showSelectButton, showAllButton, endButton;
	JTextArea textArea;
	JTextField teamNameTextField, cmdResultTextField;
	
	public MainMenu() {
		JFrame frame = new JFrame("YHJ Inc. ERP Program");
		addButton = new JButton("1. 부서 정보 추가");
		deleteButton = new JButton("2. 부서 삭제");
		updateButton = new JButton("3. 부서 정보 수정");
		showSelectButton = new JButton("4. 조회 부서 출력");
		showAllButton = new JButton("5. 전체 부서 출력");
		endButton = new JButton("6. 종료");
		teamNameTextField = new JTextField();
		cmdResultTextField = new JTextField();
		JLabel label = new JLabel("삭제/수정/출력할 부서명 입력:");
		
		textArea = new JTextArea();
		textArea.setAutoscrolls(true);
		textArea.setEditable(false);
		
		JPanel menuPanel = new JPanel();
		JPanel textPanel = new JPanel();
		JPanel topPanel = new JPanel();
		
		topPanel.setLayout(new GridLayout(1,2));
		topPanel.add(label);
		topPanel.add(teamNameTextField);
		
		menuPanel.setLayout(new GridLayout(6, 1));
		menuPanel.add(addButton);
		menuPanel.add(deleteButton);
		menuPanel.add(updateButton);
		menuPanel.add(showSelectButton);
		menuPanel.add(showAllButton);
		menuPanel.add(endButton);
		
		textPanel.setLayout(new BorderLayout());
		textPanel.add(topPanel, BorderLayout.NORTH);
		textPanel.add(textArea, BorderLayout.CENTER);
		
		frame.setLayout(new BorderLayout());
		frame.add(textPanel, BorderLayout.CENTER);
		frame.add(menuPanel, BorderLayout.WEST);
		frame.add(cmdResultTextField, BorderLayout.SOUTH);
		
		// --------------------[프레임 위치 설정하는 코드]----------------------
		frame.setLocationRelativeTo(null);
	    // ----------------------------------------------------------------------

		frame.setVisible(true);
		frame.setSize(550, 350);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		addButton.addActionListener(this);
		deleteButton.addActionListener(this);
		updateButton.addActionListener(this);
		showSelectButton.addActionListener(this);
		showAllButton.addActionListener(this);
		endButton.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		TeamDataController dco = new TeamDataController();
		
		if(e.getSource() == addButton) {		
			new InputInformation("$add$");		
			teamNameTextField.setText(null);
			
		} else if(e.getSource() == deleteButton) {		
			dco.deleteTeam(teamNameTextField.getText());
			teamNameTextField.setText(null);
			
		} else if(e.getSource() == updateButton) {		
			new InputInformation("update");
			teamNameTextField.setText(null);
			
		} else if(e.getSource() == showSelectButton) {	
			String result = dco.showSelectedTeam(teamNameTextField.getText());
			teamNameTextField.setText(null);
			textArea.setText(result);
			
		} else if(e.getSource() == showAllButton) {		
			String result = dco.showAll();
			teamNameTextField.setText(null);
			textArea.setText(result);
			
		} else if(e.getSource() == endButton) {
			new EndPopUp();	
			
		}
	}
	
}
