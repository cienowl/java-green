package d20200515.company_erp.ui_windows;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class EndPopUp implements ActionListener{
	JFrame frame;
	JButton yesButton, noButton;

	public EndPopUp() {		
		frame = new JFrame("부서 정보 프로그램");
		JLabel label = new JLabel("종료하시겠습니까?");
		yesButton = new JButton("예");
		noButton = new JButton("아니오");
		JPanel buttonPanel = new JPanel();
		
		buttonPanel.setLayout(new FlowLayout());
		buttonPanel.add(yesButton);
		buttonPanel.add(noButton);
		
		frame.setLayout(new GridLayout(2,1));
		frame.add(label);
		frame.add(buttonPanel);
		
		// --------------------[프레임 위치 설정하는 코드]----------------------
		frame.setLocationRelativeTo(null);
	    // ----------------------------------------------------------------------
		
		frame.setVisible(true);
		frame.setSize(200, 120);
		
		yesButton.addActionListener(this);
		noButton.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == yesButton) {
			System.exit(0);
		} else if(e.getSource() == noButton) {
			frame.dispose();
		}
	}
	
}
