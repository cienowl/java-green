package d20200515.company_erp.ui_windows;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class ErrorPopUp implements ActionListener{
	JFrame frame;
	JButton confirmButton;

	public ErrorPopUp(String errorMSG) {		
		frame = new JFrame("Error");
		JLabel label = new JLabel(errorMSG);
		confirmButton = new JButton("확인");
		JPanel buttonPanel = new JPanel();
		
		buttonPanel.setLayout(new FlowLayout());

		buttonPanel.add(confirmButton);
		
		frame.setLayout(new GridLayout(2,1));
		frame.add(label);
		frame.add(buttonPanel);
		
		// --------------------[프레임 위치 설정하는 코드]----------------------
		frame.setLocationRelativeTo(null);
	    // ----------------------------------------------------------------------
		
		frame.setVisible(true);
		frame.setSize(200, 120);
		
		confirmButton.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == confirmButton) 
			frame.dispose();
	}
}
