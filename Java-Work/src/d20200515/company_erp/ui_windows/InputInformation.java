package d20200515.company_erp.ui_windows;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import d20200515.company_erp.TeamDataController;
import d20200515.company_erp.TeamDataTransfer;

public class InputInformation implements ActionListener{
	
	JFrame frame;
	JButton confirmButton, cancelButton;
	JTextField codeText;
	JTextField nameText;
	JTextField locationText;
	JTextField telText;
	String toDo;	//toDo = $add$ or delete or update or show
	
	public InputInformation(String toDo) {
		this.toDo = toDo;	//$add$ 또는 부서명
		
		frame = new JFrame("부서 입력");
		JPanel inputPanel = new JPanel();
		JPanel buttonPanel = new JPanel();
		JLabel codeLabel = new JLabel("부서 코드: ");
		JLabel nameLabel = new JLabel("부서 이름: ");
		JLabel locationLabel = new JLabel("부서 위치: ");
		JLabel telLabel = new JLabel("부서 전화번호: ");
		codeText = new JTextField();
		nameText = new JTextField();
		locationText = new JTextField();
		telText = new JTextField();
		confirmButton = new JButton("확인");
		cancelButton = new JButton("취소");
		
		inputPanel.setLayout(new GridLayout(4,2));
		inputPanel.add(codeLabel);
		inputPanel.add(codeText);
		inputPanel.add(nameLabel);
		inputPanel.add(nameText);
		inputPanel.add(locationLabel);
		inputPanel.add(locationText);
		inputPanel.add(telLabel);
		inputPanel.add(telText);
		
		buttonPanel.setLayout(new FlowLayout());
		buttonPanel.add(confirmButton);
		buttonPanel.add(cancelButton);		
		
		frame.setLayout(new BorderLayout());
		frame.add(inputPanel, BorderLayout.CENTER);
		frame.add(buttonPanel, BorderLayout.SOUTH);
		
		// --------------------[프레임 위치 설정하는 코드]----------------------
		frame.setLocationRelativeTo(null);
	    // ----------------------------------------------------------------------
		
		frame.setVisible(true);
		frame.setSize(300,200);
		
		confirmButton.addActionListener(this);
		cancelButton.addActionListener(this);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {

		if(e.getSource() == confirmButton) {
			TeamDataController dco = new TeamDataController();
			TeamDataTransfer dto = new TeamDataTransfer();
			
			dto.setTeamCode(codeText.getText());
			dto.setTeamName(nameText.getText());
			dto.setTeamLocation(locationText.getText());
			dto.setTeamTel(telText.getText());
			
			if(toDo == "$add$") {
				dco.addTeam(dto);		
			} else {
				dco.updateTeam(dto);		//toDo = delete or update or show
			}
			
			frame.dispose();
			
		} else if(e.getSource() == cancelButton) {
			frame.dispose();
		} 
		
	}

}
