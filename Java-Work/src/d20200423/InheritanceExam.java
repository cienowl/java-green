package d20200423;

class Car {
	//private: 외부접근X
	//protected: 외부접근X, 자식클래스에서는 사용 가능
	protected String carname;	//String 형 변수 carname
	public int cost;		//int 형 변수 cost
	
	Car() {
		System.out.println("부모생성자");
	}
		
	protected void printAttributes() {		//protected 형 메소드, 리턴값 없음
		System.out.println("carname = " + carname + "\tcost = " + cost);	//출력
	}
}

class Audi extends Car {		//Car클래스 상속받음, Audi 클래스 시작
	public Audi() {				//생성자
		//부모클래스 생성자 호출 -> defualt로 실행
		carname = "Audi";		//carname 변수에 Audi 저장
		cost = 20000;			//cost 변수에 20000 저장
	}
}

class Bmw extends Car {			//Car클래스 상속받음, Bmw 클래스 시작
	int a = 10;					//int 형 변수 a 선언 후 10 저장, 자식클래스 멤버는 부모클래스에서 사용 불가능
	public Bmw() {				//생성자 Bmw
		carname = "BMW";		//carname 변수에 BMW 저장
		cost = 10000;			//cost 변수에 10000 저장
	}
}

class Benz extends Car {		//Car 클래스 상속받음, Benz 클래스 시작
	public Benz() {				//생성자 Benz
		carname = "Benz";		//carname 변수에 Benz 저장
		cost = 25000;			//cost 변수에 25000 저장
	}
}

public class InheritanceExam {
	public static void main(String[] args) {
		Car c = new Car();		//Car 클래스 객체 선언/생성
		Audi au = new Audi();	//Audi 클래스 객체 선언/생성
		Bmw bm = new Bmw();		//bm 클래스 객체 선언/생성
		Benz be = new Benz();	//be 클래스 객체 선언/생성

		System.out.println(c);	//객체 c 주소값
		System.out.println(au);	//객체 au 주소값
		System.out.println(bm);	//객체 bm 주소값
		System.out.println(be);	//객체 be 주소값

		c.printAttributes();
		au.printAttributes();
		bm.printAttributes();
		be.printAttributes();
	}
}