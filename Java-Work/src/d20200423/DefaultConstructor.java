package d20200423;

class Alpha {
	private int a;

	Alpha() {
		a = 50;
		System.out.println("AAAA");
	}

	int getA() {
		return a;
	}
}

class Beta extends Alpha {

}  

public class DefaultConstructor {
	public static void main(String[] args) {
		Beta x = new Beta();
		System.out.println("x.getA() = " + x.getA());
	}
}
