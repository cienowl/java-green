package d20200423;

abstract class Shap {
	abstract void draw();		//추상 method 생성
}

class Poin extends Shap {
	Poin() {}

	void draw() {				//Overriding 재정의, 생성시 형태와 일치해야 함
		System.out.println('+');
	}
}

class Rect extends Shap {		//Shap 클래스 상속받음
	private int width;
	private int height;

	Rect(int width, int height) {	//생성자
		this.width = width;			//생성자로 넘어온 매개변수 width를 클래스 내 width에 저장 this. 필요
		this.height = height;		//상동
	}

	void draw() {					//Overriding 재정의
		for(int i=1; i<=height; i++) {
			for(int j=1; j<=width; j++)
				System.out.print('*');
			System.out.println();
		}
	}
}

class ShapeTester {
	public static void main(String[] args) {
		Shap[] a = new Shap[2];
		a[0] = new Poin();
		a[1] = new Rect(4,3);

		for(Shap s:a) {
			s.draw();
			System.out.println();
		}
	}
}