package d20200423;

class WapperTst {
	public static void boxingUnboxing() {
		int i = 10;
		Integer in = new Integer(i);
		Boolean b = new Boolean(false);
		Long l = new Long("10");

		int j = in.intValue();
		boolean bl = b.booleanValue();
		long lg = l.longValue();
	}

	public static void autoBoxingunboxing() {
		Integer in = 10;
		Boolean bl = true;
		int i = in;
		boolean b = bl;
	}

	public static void StringToWapper() {
		int r = Integer.parseInt("2030");
		double d = Double.parseDouble("2030.12");
		boolean b = Boolean.parseBoolean("false");
		float f = Float.parseFloat("abcde");
//		char c = Character.parseChar("a");
	}

	public static void main(String[] args) {
	}
}
