package d20200423;

class Sup {
	public Sup() {
		System.out.println(1);	//<1>
	}
	public Sup(int i) {
		System.out.println(2);	//<2>
	}
	public Sup(boolean b) {
		System.out.println(3);	//<3>
	}
}

class Sub extends Sup {
	public Sub() {
		super(3);		//생성자 int 형 매개변수 찾음
		System.out.println(4);	//<4>
	}
	public Sub(int a) {
		this("A");
		System.out.println(5);	//<5>
	}
	public Sub(String s) {
		super(true);
		System.out.println(6);	//<6>
	}
}

class ConstructorExam {
	public static void main(String[] args) {
//		Sub a = new Sub();			// <4> 실행 -> <1> 호출: 부모 생성자 / super(3) 으로 <2> 호출 ==> 2 -> 4 출력
//		Sub b = new Sub(3);			// <5> 실행 -> this 로 <6> 실행 -> super로 3 실행 ==> 3 -> 6 -> 5 출력
//		Sub c = new Sub(true);		// 오류 Sub클래스에 boolean 가능 생성자 없음
//		Sub d = new Sub("A");		// <6> 실행 -> super로 인해 <3> 실행 ==> 3 -> 6 출력
	}
}
