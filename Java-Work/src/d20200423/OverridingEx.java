package d20200423;

/*
오버라이딩(재정의) : 부모클래스안에 정의된 메소드를 자식클래스에서 새롭게 정의해서 사용하는 것
오버로딩 : 하나의 클래스에서 같은 이름(인수는 무조건 달라야함!!) 메소드를 여러개 만들어 내는 것.
*/

class A {
	int i;
	void method() {
		System.out.println("aa");
	}
}

class B extends A {
	void method() {					//오버라이딩(재정의) : 부모클래스 안에 정의된 메소드를 자식 클래스에서
		System.out.println("dd");	//새롭게 정의해서 사용하는 것.
	}
}

class OverridingEx{
	public static void main(String[] args) {
		System.out.println("Hello World!");
	}
}
