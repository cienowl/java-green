package d20200423;

class SuperClass {
	int num;
	String str = "String in SuperClass";
	SuperClass() {
		System.out.println("생성자 SuperClass() 호출");
	}

	public void methodZero() {
		System.out.println("methodZero() 메소드");
		System.out.println("num = " + num + "\t" + "str = " + str ); 
	}
	
	void methodOne() {
		System.out.println(" methodOne() 메소드");
		System.out.println("num = " + num + "\t" + "str = " + str);
	}
}

class SubClass extends SuperClass {
	SubClass() {
		str = "SubClass의 생성자가 입력한 String";
		System.out.println("생성자 SubClass() 호출");
	}

	public void methodZero() {
		System.out.println("SubClass 객체 methodZero() 메소드");
		System.out.println("num = " + num + "\t" + "str = " + str);
	}
	
	int methodTwo(int a) {
		num = a;
		System.out.println("SubClass객체 methodTwo().");
		System.out.println("num = " + num + "\t" + "str = " + str);
	
		return num;
	}

	String methodThree(String s, int k) {
		num = k;
		str = s;
		System.out.println("SubClass객체 methodThree() 메소드");
		System.out.println("num = " + num + "\t" + "str = " + str);

		return str;
	}

	protected String methodThree(int k, String str) {
		num = k;
		str = str;
		System.out.println("SubClass객체 methodThree()");
		System.out.println("num = " + num + "\t" + "str = " + str);

		return str;
	}
}

class MethodOverridingExam {
	public static void main(String[] args) {
		SuperClass sp = new SuperClass();
		System.out.println("***********************************");
		SubClass sb = new SubClass();
		System.out.println("***********************************");
		
		sp.methodZero();
		sp.methodOne();
//		sp.methodTwo(2);
//		sp.methodThree(3, "MethodOverriding");		//부모클래스는 자식 클래스 메소드 사용불가
		System.out.println("***********************************");

		sb.methodZero();
		sb.methodOne();
		sb.methodTwo(2222);
		sb.methodThree(3333, "또 MethodOverriding");
		sb.methodThree("MethodOverriding", 5555);

	}
}
