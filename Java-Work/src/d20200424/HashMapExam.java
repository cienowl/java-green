package d20200424;

import java.util.HashMap;
import java.util.Set;

public class HashMapExam extends HashMap<String, Integer> {

	public HashMapExam() {
//		super();				//super() 가 자동 실행. 부모 클래스의 생성자
		super.put("a", 10);		//super는 생략 가능함. 상속을 나타내기 위해 명시적으로 사용
		super.put("b", 20);
		super.put("c", 30);

		System.out.println("size = " + super.size());

		Set<String> s = super.keySet();		//Map에 있는 키들을 반환 리턴값은 Set 이므로 Set 사용 <E> 는 String
		for (String str : s) {				//s의 개수를 알 수 없기 때문에 개선된 포문 사용
			int i = super.get(str);			//
			System.out.println("key = " + str + "value" + i);
		}
	}

	public static void main(String[] args) {
		new HashMapExam();
	}
}
