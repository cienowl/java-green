package d20200424;

import java.util.Enumeration;
import java.util.Hashtable;

class UserInfo {
	String name;
	String addr;
	String tel;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;	//나의 name 에 매개변수를 대입
	}
	public String getAddr() {
		return addr;
	}
	public void setAddr(String addr) {
		this.addr = addr;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
}

public class CollectionMap {
	Hashtable<String, UserInfo> table = new Hashtable<String, UserInfo>();

	public CollectionMap() {
//		UserInfo info = new UserInfo();
//		ArrayList<UserInfo> arrlist = new ArrayList<UserInfo>();	//ArrayList 에 오브젝트 타입을 객체화 된 클래스를 넣어 쓸수 있음.
//		HashMap<String,UserInfo> hashmap = new HashMap<>();			//HashMap 또한 오브젝트 타입을 객체화 해서 사용 가능.
//		arrlist.add(info);
//		
//		ArrayList<String> arrlist = new ArrayList<String>();
//		String a = "abc";
//		arrlist.add(a);
		
		UserInfo u = new UserInfo();	//UserInfo 형 u 객체 생성
		u.setName("나");
		u.setAddr("강남구");
		u.setTel("010");
		
		UserInfo u2 = new UserInfo();	//UserInfo 형 u2 객체 생성
		u2.setName("이나영");
		u2.setAddr("역삼동");
		u2.setTel("1234");
		
		table.put("na", u);		//Hash 테이블에 na 키값으로 u 객체 저장
		table.put("lee", u2);	//Hash 테이블에 lee 키값으로 u2 객체 저장
		
		Enumeration<String> e = table.keys();	//An object that implements the Enumeration interface generates a series of elements, one at a time. Successive calls to the nextElement method return successive elements of theseries. 
		while(e.hasMoreElements()) {
			String key = e.nextElement();		//문자열형 key 변수에 Returns the next element of this enumeration if this enumeration object has at least one more element to provide.
			UserInfo value = table.get(key);	//키와 매핑 되어있는 값을 value에 저장 
			
			System.out.println(key + " = " + value);	//키값 출력
			System.out.println(value.getName() + "," + value.getTel() + "," + value.getAddr());
		}
		
	}
	
	public static void main(String[] args) {
		new CollectionMap();
	}

}
