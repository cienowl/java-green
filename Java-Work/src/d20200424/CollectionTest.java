package d20200424;

import java.util.ArrayList;
import java.util.Collections;

public class CollectionTest {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) {
		ArrayList<Integer> list1 = new ArrayList<Integer>(10);	//Integer 오브젝트 타입 ArrayList 객체 list1 생성 size = 10
		list1.add(5);	//리스트 객체에 숫자 5 추가
		list1.add(4);	//리스트 객체에 숫자 4 추가
		list1.add(2);	//리스트 객체에 숫자 2 추가
		list1.add(0);	//리스트 객체에 숫자 0 추가
		list1.add(1);	//리스트 객체에 숫자 1 추가
		list1.add(3);	//리스트 객체에 숫자 3 추가
		
		ArrayList list2 = new ArrayList(list1.subList(1, 4));	//list1 객체의 1번 인덱스 위치부터 4번인덱스 위치전까지 element 개수 만큼 list2 생성
		print(list1, list2);
		
		Collections.sort(list1);		//list1 객체 오름차순 정렬
		Collections.sort(list2);		//list2 객체 오름차순 정렬
		print(list1, list2);
		
		System.out.println("list1.containsAll(list2)" + list1.containsAll(list2));	//true if this collection contains all of the elements in the specified collection
		list2.add("B");			//list2에 B 추가
		list2.add("C");			//list2에 C 추가
		list2.add(3,"A");		//list2 3번 인덱스에 A를 insert
		print(list1, list2);
		
		list2.set(3, "AA");		//list2 3번 인덱스에 AA로 값을 바꿈
		print(list1, list2);
		
		System.out.println("list1.retainAll(list2)" + list1.retainAll(list2));	//list2 내용과 비교하여 값은 element 이외의 모든 것을 삭제 후 같은 값이 있으면 true 리턴
		print(list1, list2);
		System.out.println(list1.size());
		
		for(int i=list2.size()-1; i>=0; i--) {	//list2의 size 값 int 형으로 리턴 5번 인덱스부터 0번까지 반복
			if(list1.contains(list2.get(i)))	//list2의 해당 인덱스 element를 가져오면서 list1에 같은 element가 있다면
				list2.remove(i);				//list2에서 해당 인덱스 내용 삭제
		}
		print(list1, list2);

	}
	
	@SuppressWarnings("rawtypes")
	static void print(ArrayList list1, ArrayList list2) {
		System.out.println("list1"+list1);
		System.out.println("list2"+list2);
		System.out.println();
	}

}
