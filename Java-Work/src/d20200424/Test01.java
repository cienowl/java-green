/*
 * 1.	ArrayList 6개의 공간생성
 * 2.	난수를 1~45사이값을 발생하여 발생한 난수를 ArrayList에 저장한다.
 * 		단 발생한 난수가 ArrayList 에 저장된 값과 중복 된다면 다시 발생하여 저장한다.
 * 3.	ArrayList에 저장된 난수의 값들을 오름차순으로 정렬한다.
 * 4.	ArrayList에 들어있는 숫자 6개를 출력한다. (출력결과는 중복없이 오름차순으로 나와야 함)
 */

package d20200424;

import java.util.ArrayList;
import java.util.Collections;

public class Test01 {

	public static void main(String[] args) {
		Integer rndNum = 0;			// Integer 형 오브젝트 rndNum 선언
		int i = 0, listSize=45;

		ArrayList<Integer> rndNumList = new ArrayList<Integer>(listSize);		// Integer 형 ArrayList 크기가 6인 rndNumList 객체 생성

		while (rndNumList.size() < listSize) {			// rndNumList 사이즈가 6이 될 때까지 반복

			rndNum = (int) (Math.random() * 45) + 1;	// rndNum 인트형 오브젝트에 1~45 사이의 난수를 저장

			if (!rndNumList.contains(rndNum))		// rndNumList에 발생된 난수가 있지 않으면
				rndNumList.add(rndNum);				// rndNumList에 숫자 추가

			i++;
		}

		System.out.println("Before ascending order: " + rndNumList);

		Collections.sort(rndNumList);				// rndNumList 의 내용을 오름차순 정렬

		System.out.println("After ascending order: " + rndNumList);

	}

}
