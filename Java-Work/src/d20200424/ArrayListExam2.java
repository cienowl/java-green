package d20200424;

import java.util.ArrayList;
import java.util.Collections;

public class ArrayListExam2 {//extends ArrayList<Integer>{

	public static void main(String[] args) {
		
		Integer rndNum = 0; // Integer 형 오브젝트 rndNum 선언
		int i = 0, listSize = 6;

		ArrayList<Integer> rndNumList = new ArrayList<Integer>(listSize); // Integer 형 ArrayList 크기가 6인 rndNumList 객체 생성

		while (rndNumList.size() < listSize) { // rndNumList 사이즈가 6이 될 때까지 반복

			rndNum = (int) (Math.random() * 45) + 1; // rndNum 인트형 오브젝트에 1~45 사이의 난수를 저장

			if (!rndNumList.contains(rndNum)) // rndNumList에 발생된 난수가 있지 않으면
				rndNumList.add(rndNum); // rndNumList에 숫자 추가

			i++;
		}

		System.out.println("Before ascending order: " + rndNumList);

		Collections.sort(rndNumList); // rndNumList 의 내용을 오름차순 정렬

		System.out.println("After ascending order: " + rndNumList);
	}

}
