package d20200427;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

class StdtInfo{
	private String name;
	private String hakbun;
	private int htmlScore, jspScore, springScore, totalScore=0;
	private double avgScore;
	
	StdtInfo(){
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHakbun() {
		return hakbun;
	}

	public void setHakbun(String hakbun) {
		this.hakbun = hakbun;
	}

	public int getHtmlScore() {
		return htmlScore;
	}

	public void setHtmlScore(int htmlScore) {
		this.htmlScore = htmlScore;
	}

	public int getJspScore() {
		return jspScore;
	}

	public void setJspScore(int jspScore) {
		this.jspScore = jspScore;
	}

	public int getSpringScore() {
		return springScore;
	}

	public void setSpringScore(int springScore) {
		this.springScore = springScore;
	}
	
	public int getTotalScore() {
		return totalScore;
	}

	public void setTotalScore(int totalScore) {
		this.totalScore = totalScore;
	}

	public double getAvgScore() {
		return avgScore;
	}

	public void setAvgScore(double avgScore) {
		this.avgScore = avgScore;
	}
	
}

class ProgController{
	int selecNum=0, i;
	boolean hasTable = false;

	ArrayList<StdtInfo> list = new ArrayList<StdtInfo>();
	StdtInfo input = new StdtInfo();
	Scanner sc = new Scanner(System.in);
	
	public boolean setInfo() {		//정보 입력
		
		System.out.println("======== 정보를 입력하세요. ========");
		System.out.print("Name: ");
		input.setName(sc.next());
		System.out.print("Hakbun: ");
		input.setHakbun(sc.next());
		System.out.print("html Score: ");
		input.setHtmlScore(sc.nextInt());
		System.out.print("jsp Score: ");
		input.setJspScore(sc.nextInt());
		System.out.print("spring Score: ");
		input.setSpringScore(sc.nextInt());
		System.out.println("==== 성적 입력이 완료되었습니다. ====\n");
		
		list.add(input);		//StdtInfo 클래스 객체를 list에 추가
		
		return true;	//메소드 실행 완료 후 true 값 리턴
		
	}
	
	public boolean calcScore() {		//점수 계산
		
		for(i = 0; i<list.size(); i++) {
			input.setTotalScore(list.get(i).getHtmlScore()+list.get(i).getJspScore()+list.get(i).getSpringScore());
			input.setAvgScore(list.get(i).getTotalScore() / 3.0);
		}
		
		System.out.println("***** 성적 계산이 완료 되었습니다. *****\n");

		return true;
	}
	
	public void printInfo() {			//출력
		System.out.println("==================== Sungjuk Output ====================");
		System.out.println("Name\tHakbun\tHTML\tJSP\tSpring\tTotal\tAvg");
		System.out.println("--------------------------------------------------------");
		
		System.out.println(list.get(list.size()-1).getName() + " \t" + list.get(list.size()-1).getHakbun() + " \t" + list.get(list.size()-1).getHtmlScore() + " \t" + list.get(list.size()-1).getJspScore() + " \t" + list.get(list.size()-1).getSpringScore() + " \t" + list.get(list.size()-1).getTotalScore() + " \t" + list.get(list.size()-1).getAvgScore());
		
		System.out.println("========================================================\n");
		
	}
	
	public void printInfoAll() {		//리스트 전체 출력
		System.out.println("==================== Sungjuk Output ====================");
		System.out.println("Name\tHakbun\tHTML\tJSP\tSpring\tTotal\tAvg");
		System.out.println("--------------------------------------------------------");

		for(i = 0; i<list.size(); i++)
			System.out.println(list.get(i).getName() + " \t" + list.get(i).getHakbun() + " \t" + list.get(i).getHtmlScore() + " \t" + list.get(i).getJspScore() + " \t" + list.get(i).getSpringScore() + " \t" + list.get(i).getTotalScore() + " \t" + list.get(i).getAvgScore());
		
		System.out.println("========================================================\n");
		
	}
	
	public void searchStudent(String search) {
		System.out.println("==================== Sungjuk Output ====================");
		System.out.println("Name\tHakbun\tHTML\tJSP\tSpring\tTotal\tAvg");
		System.out.println("--------------------------------------------------------");

		for(i = 0; i<list.size(); i++) {
			if(search.equals(list.get(i).getName()))
				System.out.println(list.get(i).getName() + " \t" + list.get(i).getHakbun() + " \t" + list.get(i).getHtmlScore() + " \t" + list.get(i).getJspScore() + " \t" + list.get(i).getSpringScore() + " \t" + list.get(i).getTotalScore() + " \t" + list.get(i).getAvgScore());
		}
		
		System.out.println("========================================================\n");
	}
	
	public void mainMenu() {
		
		while(selecNum != 6) {		//selecNum이 6이 되면 반복문 종료
			
			System.out.println("***** 성적처리 프로그램 Ver.0.0.2 *****\n1. 입력\n2. 계산\n3. 출력\n4. 전체출력\n5. 검색\n6. 종료");
			System.out.print("- 당신이 원하는 메뉴를 선택세요: ");

			try {		//예외처리
				selecNum = sc.nextInt();	//선택 숫자 스캔
			} catch (InputMismatchException e) {	//숫자 외 값 입력시 에러발생
				System.out.println("Error! - 정수로 입력하세요. ");
				selecNum = 6;
			}
			
			System.out.println();
			
			switch(selecNum) {
			case 1:	//입력
				hasTable = setInfo();		//setInfo 메소드 호출 후 리턴값 저장
				break;
			case 2:	//계산
				if(hasTable)
					calcScore();
				else
					System.out.println("Error! - 성적이 입력되지 않았습니다.\n");
				break;
			case 3:	//출력
				if(hasTable)
					printInfo();
				else
					System.out.println("Error! - 성적이 입력되지 않았습니다.\n");
				break;
			case 4:	//전체출력
				if(hasTable)
					printInfoAll();
				else
					System.out.println("Error! - 성적이 입력되지 않았습니다.\n");
				break;
			case 5:	//검색
				if(hasTable) {
					System.out.print("검색할 학생의 이름을 입력하세요: ");
					searchStudent(sc.next());
				} else {
					System.out.println("Error! - 성적이 입력되지 않았습니다.\n");
				}				
				break;
			case 6:	//종료
				System.out.println("프로그램이 종료되었습니다.");
				sc.close();
				break;
			default:
				System.out.println("Error! - 1~6번 중에서 선택하세요.\n");
				break;
				
			}
			
		}
		
	}
	
}

public class SungjukProgramArrayList {

	public static void main(String[] args) {

		ProgController startProgram = new ProgController();		//프로그램 컨트롤러 클래스 객체 생성
		startProgram.mainMenu();		//프로그램 스타트

	}

}
