package d20200427;

public class TryCatchLearn {

	public static void main(String[] args) {
			
		try {	//예외가 발생될 것 같은 위치
//			String[] s  = new String[3];
//			s[3] = "aaa";
			String str = "abc";
			System.out.println(str);
			int i = Integer.parseInt(str);
			int result = 10/i;
			
//		} catch(ArrayIndexOutOfBoundsException e) {		//try가 있으면 catch는 무조건 있어야함. catch는 간단한 내용
//			System.out.println(e + " => 인수를 꼭 넣어주세요.");
//		} catch(NumberFormatException e) {
//			System.out.println(e + " => 숫자만 입력하세요.");
		} catch(Exception e) {	//exception 최상위
			System.out.println(e + " => 오류입니다.");
		} finally {	//없어도 됨. 예외 발생/미발생 모두 작동
			System.out.println("난 무조건(예외가 발생하거나 안하거나) 실행합니다.");
		}		//오류가 나도 끝까지 진행
		System.out.println("** 프로그램 종료 **");

	}

}
