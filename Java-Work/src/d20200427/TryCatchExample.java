package d20200427;

public class TryCatchExample {
	
	void method() throws Exception{		//문장안에서 아무곳에서 예외 발생시 무조건 받아냄
		
	}
	
	public static void main(String[] args) {
		
//		try {	//메소드 전체 예외처리는 호출시 try 로 감싸놓음.
//			ob.method();
//		} catch() {		
//		}
		
		int i = (int)(Math.random()*3)+1;
		System.out.println("난수 = "+i);
		
		if(i==3) {
			try {
				throw new Exception();		//exception 강제 발생 throw
			} catch(Exception e) {			//catch에서 예외 문장 받아서 저장
				System.out.println(e + "=>발생");		//예외 문장 출력
			}
		} else {
			System.out.println("올바른 숫자입니다.");
		}
		System.out.println("*** End ***");
	}
	
}
