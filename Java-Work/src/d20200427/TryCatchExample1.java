package d20200427;

class Score{
	private int score;		//private 정수형 score 선언
	
	public void setS(int j) throws MyEx{
		if(j<0 || j>100)
			throw new MyEx();		//매개변수 j 값이 0~100 사이 값이 아니면 예외 발생 MyEx() 메소드 호출
		else						//throw 후 new 에 들어올수 있는 클래스는 예외클래스만 가능 따라서 해당 클래스 exception 상속 받아야함
			this.score = j;			//매개변수 j 값이 0~100 사이의 값이면 score 변수에 저장
	}
}

public class TryCatchExample1 {

	public static void main(String[] args) {
		
		Score a = new Score();		//Score 클래스, a 객체 생성
		try {
			a.setS(110);			//객체 a의 setS 메소드에 매개변수값 110을 가지고 호출
		} catch(MyEx ex) {
			System.out.println("오류");
		}		

	}

}

class MyEx extends Exception {		//exception 을 상속 받아서 새로운 오류를 만들어냄
	MyEx() {						//MyEx 클래스 생성자
		System.out.println("error");	//error 출력
	}
}
