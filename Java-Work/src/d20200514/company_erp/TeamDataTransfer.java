package d20200514.company_erp;

public class TeamDataTransfer {
	private String teamCode;
	private String teamName;
	private String teamLocation;
	private String teamTel;
	
	public String getTeamCode() {
		return teamCode;
	}
	public void setTeamCode(String teamCode) {
		this.teamCode = teamCode;
	}
	public String getTeamName() {
		return teamName;
	}
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
	public String getTeamLocation() {
		return teamLocation;
	}
	public void setTeamLocation(String teamLocation) {
		this.teamLocation = teamLocation;
	}
	public String getTeamTel() {
		return teamTel;
	}
	public void setTeamTel(String teamTel) {
		this.teamTel = teamTel;
	}
		
}
