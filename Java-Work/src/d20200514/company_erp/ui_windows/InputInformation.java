package d20200514.company_erp.ui_windows;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class InputInformation implements ActionListener{
	
	//	selectTeamText() 버튼
	JButton confirmButton, cancelButton;
	
	public InputInformation() {
		JFrame frame = new JFrame("부서 입력");
		JPanel inputPanel = new JPanel();
		JPanel buttonPanel = new JPanel();
		JLabel codeLabel = new JLabel("부서 코드: ");
		JLabel nameLabel = new JLabel("부서 이름: ");
		JLabel locationLabel = new JLabel("부서 위치: ");
		JLabel telLabel = new JLabel("부서 전화번호: ");
		JTextField codeText = new JTextField();
		JTextField nameText = new JTextField();
		JTextField locationText = new JTextField();
		JTextField telText = new JTextField();
		confirmButton = new JButton("확인");
		cancelButton = new JButton("취소");
		
		inputPanel.setLayout(new GridLayout(4,2));
		inputPanel.add(codeLabel);
		inputPanel.add(codeText);
		inputPanel.add(nameLabel);
		inputPanel.add(nameText);
		inputPanel.add(locationLabel);
		inputPanel.add(locationText);
		inputPanel.add(telLabel);
		inputPanel.add(telText);
		
		buttonPanel.setLayout(new FlowLayout());
		buttonPanel.add(confirmButton);
		buttonPanel.add(cancelButton);		
		
		frame.setLayout(new BorderLayout());
		frame.add(inputPanel, BorderLayout.CENTER);
		frame.add(buttonPanel, BorderLayout.SOUTH);
		
		frame.setVisible(true);
		frame.setSize(300,200);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
	//	TeamDataController dco = new TeamDataController();
		
		if(e.getSource() == confirmButton) {
			System.exit(0);
		} else if(e.getSource() == cancelButton) {
			System.out.println("no");
		} 
		
	}

}
