package d20200514.company_erp.ui_windows;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import d20200514.company_erp.TeamDataController;

public class MainMenu implements ActionListener{
	JButton addButton, deleteButton, updateButton, showSelectButton, showAllButton, endButton;
	
	public MainMenu() {
		JFrame frame = new JFrame("YHJ Inc. ERP Program");
		addButton = new JButton("1. 부서 정보 입력");
		deleteButton = new JButton("2. 부서 삭제");
		updateButton = new JButton("3. 부서 정보 수정");
		showSelectButton = new JButton("4. 조회 부서 출력");
		showAllButton = new JButton("5. 전체 부서 출력");
		endButton = new JButton("6. 종료");
		JTextField textField = new JTextField();
		
		JPanel menuPanel = new JPanel();
		menuPanel.setLayout(new GridLayout(7, 1));
		menuPanel.add(addButton);
		menuPanel.add(deleteButton);
		menuPanel.add(updateButton);
		menuPanel.add(showSelectButton);
		menuPanel.add(showAllButton);
		menuPanel.add(endButton);
		menuPanel.add(textField);
		
		frame.setLayout(new BorderLayout());
		frame.add(new JTextArea(), BorderLayout.CENTER);
		frame.add(menuPanel, BorderLayout.WEST);
		
		// --------------------[프레임 위치 설정하는 코드]----------------------
		frame.setLocationRelativeTo(null);
	    // ----------------------------------------------------------------------

		frame.setVisible(true);
		frame.setSize(600, 400);
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		
		addButton.addActionListener(this);
		deleteButton.addActionListener(this);
		updateButton.addActionListener(this);
		showSelectButton.addActionListener(this);
		showAllButton.addActionListener(this);
		endButton.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		TeamDataController dco = new TeamDataController();
		
		if(e.getSource() == addButton) {
			dco.addTeam();
		} else if(e.getSource() == deleteButton) {
			dco.deleteTeam();
		} else if(e.getSource() == updateButton) {
			new TeamInputPopUp();
		} else if(e.getSource() == showSelectButton) {
			new TeamInputPopUp();
		} else if(e.getSource() == showAllButton) {
			dco.showAll();
		} else if(e.getSource() == endButton) {
			new EndPopUp();		//두번 누르면 삭제				
		}
	}
	
}
