package d20200514.company_erp.ui_windows;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class TeamInputPopUp implements ActionListener{
	//	inputInfo() 버튼
	JButton confirmButton, cancelButton;
	
	public TeamInputPopUp() {
		JFrame frame = new JFrame("정보 입력");
		JTextField textField = new JTextField(12);
		JLabel label = new JLabel("부서입력: ");
		confirmButton = new JButton("확인");
		cancelButton = new JButton("취소");
		JPanel buttonPanel = new JPanel();
		JPanel inputPanel = new JPanel();
		
		inputPanel.setLayout(new FlowLayout());
		inputPanel.add(label);
		inputPanel.add(textField);
		
		buttonPanel.setLayout(new FlowLayout());
		buttonPanel.add(confirmButton);
		buttonPanel.add(cancelButton);
		
		frame.setLayout(new GridLayout(2,1));
		frame.add(inputPanel);
		frame.add(buttonPanel);
		
		frame.setVisible(true);
		frame.setSize(250, 120);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	
		
	}
	
	
}
