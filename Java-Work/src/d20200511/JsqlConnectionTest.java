/*
 * MySql 문법
 * show databases; 		//모든 database 보여줌
 * create database [database_name];	//database 생성
 * use [database_name]	//database_name 의 db 사용 시작
 * show tables;			//db 안의 모든 table을 보여줌
 * create table
 * desc [table_name];	//테이블 내의 필드 속성 출력
 * create table table_name('field' type(size), 'field2' type(size), ...);	//type은 varchar, int, date 만 존재 괄호안에는 사이즈 지정
 * insert into [table_name] values(field1, field2, ...);	//테이블에 field값 입력
 * select * from [table_name];			//해당 테이블의 모든 필드값 호출
 * select field1, fiedl2, ... from [table_name];	//해당 테이블의 지정된 필드값 호출, 순서대로 1, 2, 3으로 access
 * select * from table_name where field1='value';	//해당 테이블 내 필드명의 value를 가진 값만 호출
 * delete from table_name;		//해당 테이블의 모든 자료를 지움
 * delete from table_name where field = value;	//테이블의 지정필드의 value값을 가진 행을 삭제
 */

package d20200511;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JsqlConnectionTest {

	public static void main(String[] args) {
		
		Connection conn = null;
		Statement stmt = null;
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost/db_d0511", "root", "34811005");	//db_d0511 데이터베이스 연결
			System.out.println("연결 성공");
			
			stmt = conn.createStatement();
			String sql = "select * from member;";	//db_d0511 데이터베이스에서 member 테이블의 모든 값 호출
			
			ResultSet rs = stmt.executeQuery(sql);
			
			while(rs.next()) {
				System.out.println(rs.getString(1));
				System.out.println(rs.getString(2));
				System.out.println(rs.getString(3));
				System.out.println(rs.getString(4));
				System.out.println(rs.getString(5));
				System.out.println(rs.getString(6));
				System.out.println(rs.getString(7));
				System.out.println(rs.getString(8));
				System.out.println(rs.getString(9));
				System.out.println(rs.getString(10));
				System.out.println(rs.getString(11));
				System.out.println(rs.getString(12));
				System.out.println(rs.getString(13));
				System.out.println(rs.getString(14));
				System.out.println(rs.getString(15));
				System.out.println();
			}
			
			String sql1 = "select * from member where id='david';";
			rs = stmt.executeQuery(sql1);
			while(rs.next()) {
				System.out.println(rs.getString(1));
				System.out.println(rs.getString(2));
				System.out.println(rs.getString(3));
				System.out.println(rs.getString(4));
				System.out.println(rs.getString(5));
				System.out.println(rs.getString(6));
				System.out.println(rs.getString(7));
				System.out.println(rs.getString(8));
				System.out.println(rs.getString(9));
				System.out.println(rs.getString(10));
				System.out.println(rs.getString(11));
				System.out.println(rs.getString(12));
				System.out.println(rs.getString(13));
				System.out.println(rs.getString(14));
				System.out.println(rs.getString(15));
				System.out.println();
			}
			
			
			
		} catch(ClassNotFoundException e) {
			System.out.println("드라이버 로딩 실패");
		} catch(SQLException e) {
			System.out.println("에러" + e);
		} finally {
			try {
				if(conn != null) {
					conn.close();
				}
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}

	}

}
