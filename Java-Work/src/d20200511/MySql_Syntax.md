---
# MySql 문법
===
## show databases;
모든 database 보여줌
## create database [database_name];
database 생성
## use [database_name];
database_name 의 DB 사용 시작
## show tables;
db 안의 모든 table을 보여줌
## create table table_name;
테이블 생성
## desc [table_name];
테이블 내의 필드 속성 출력
## create table table_name('field' type(size), 'field2' type(size), ...);
type은 varchar, int, date 만 존재 괄호안에는 사이즈 지정
## insert into [table_name] values(field1, field2, ...);
테이블에 field값 입력
## select * from [table_name];
해당 테이블의 모든 필드값 호출
## select field1, fiedl2, ... from [table_name];
해당 테이블의 지정된 필드값 호출, 순서대로 1, 2, 3으로 access
## select * from table_name where field1='value';
해당 테이블 내 필드명의 value를 가진 값만 호출
## delete from table_name;
해당 테이블의 모든 자료를 지움
---