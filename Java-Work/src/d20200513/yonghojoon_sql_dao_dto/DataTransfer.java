package d20200513.yonghojoon_sql_dao_dto;

public class DataTransfer {
/* database yonghojoon, table user;
+-------------+-------------+------+-----+---------+-------+
| Field       | Type        | Null | Key | Default | Extra |
+-------------+-------------+------+-----+---------+-------+
| id          | varchar(20) | YES  |     | NULL    |       |
| password    | varchar(20) | YES  |     | NULL    |       |
| name        | varchar(10) | YES  |     | NULL    |       |
| birthday    | date        | YES  |     | NULL    |       |
| email       | varchar(50) | YES  |     | NULL    |       |
| nickname    | varchar(20) | YES  |     | NULL    |       |
| phone       | varchar(20) | YES  |     | NULL    |       |
| signon_date | date        | YES  |     | NULL    |       |
| gender      | int(11)     | YES  |     | NULL    |       |
| info_agree  | int(11)     | YES  |     | NULL    |       |
+-------------+-------------+------+-----+---------+-------+
 */
	
	private String id, password, name, email, nickname, phone, birthday, signon_date;
	private int gender, info_agree;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getSignon_date() {
		return signon_date;
	}
	public void setSignon_date(String signon_date) {
		this.signon_date = signon_date;
	}
	public int getGender() {
		return gender;
	}
	public void setGender(int gender) {
		this.gender = gender;
	}
	public int getInfo_agree() {
		return info_agree;
	}
	public void setInfo_agree(int info_agree) {
		this.info_agree = info_agree;
	}
		
}
