package d20200513.yonghojoon_sql_dao_dto;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DataAccess {

	private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	private static final String JDBC_URL = "jdbc:mysql://localhost/yonghojoon";
	private static final String USER = "root";
	private static final String PASSWD = "sjrnfl12^^";
	private Connection conn = null;
	private Statement stmt = null;
	
	public DataAccess() {
		try {
			Class.forName(JDBC_DRIVER);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void connectSql() {
		
		try {
			conn = DriverManager.getConnection(JDBC_URL, USER, PASSWD);			
			stmt = conn.createStatement();
		} catch(Exception e) {
			e.printStackTrace();
		} 
		
	}
	
	public void disconnSql() {
		if(stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} 
		if(conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public ArrayList<String> CheckIdRequest() {
		ArrayList<String> idList = new ArrayList<String>();
		
		String sqlCommand = "select id from user";
		
		connectSql();
		
		ResultSet rs = null;
		try {
			
			rs = stmt.executeQuery(sqlCommand);
			
			int idx = 0;
			while(rs.next()) {
				idList.add(idx, rs.getString(1));
				idx++;
			}
			
		}  catch(SQLException e) {
			System.out.println("에러" + e);
		} finally {
		
			disconnSql();
		}

		return idList;
	}
	
	public DataTransfer selectSql(String input_id) {	

		DataTransfer dto = new DataTransfer();
		
		ResultSet rs = null;
				
		connectSql();
		String sqlCommand = "select * from user where id='"+input_id+"';";
		
		try {
			
			rs = stmt.executeQuery(sqlCommand);
			
			if(rs.next()) {				
				dto.setId(rs.getString(1));
				dto.setPassword(rs.getString(2));
				dto.setName(rs.getString(3));
				dto.setBirthday(rs.getString(4));
				dto.setEmail(rs.getString(5));
				dto.setNickname(rs.getString(6));
				dto.setPhone(rs.getString(7));
				dto.setSignon_date(rs.getString(8));
				dto.setGender(rs.getInt(9));
				dto.setInfo_agree(rs.getInt(10));
			} else {
				dto = null;
			}
			
		} catch(SQLException e) {
			System.out.println("에러" + e);
		} finally {
			disconnSql();
		}
		
		return dto;
		
	}
	
	public void updateSql(String sqlCommand) {
		
		connectSql();
		
		try {
			
			stmt.executeUpdate(sqlCommand);
			System.out.println("SQL Update Completed.\n");
			
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			disconnSql();
		}
		
	}
	
	public void insertSql(DataTransfer inputData) {
		DataTransfer dto = inputData;
		
		connectSql();
		
		String sqlCommand = "insert into user values('"+dto.getId()+"','"+dto.getPassword()+"','"+dto.getName()+"','"+dto.getBirthday()+"','"+dto.getEmail()+"','"+dto.getNickname()+"','"+dto.getPhone()+"','"+dto.getSignon_date()+"',"+dto.getGender()+","+dto.getInfo_agree()+")";
		
		try {
			
			stmt.executeUpdate(sqlCommand);
				
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			disconnSql();
		}
		
	}
	
	public void deleteSql(String deleteId) {
		
		connectSql();
		
		String sqlCommand = "delete from user where id = '"+deleteId+"';";
		
		try {
			
			stmt.executeUpdate(sqlCommand);
			System.out.println("정보 삭제 완료\n");
			
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			disconnSql();
		}
		
	}

}
