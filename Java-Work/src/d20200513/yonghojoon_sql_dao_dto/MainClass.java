/*
 * 회원관리 프로그램
 * 
 * 1. 가입 - 이름, 생년월일, 아이디(중복확인) -> 파일출력
 * 2. 로그인 - 아이디, 비밀번호 같을 경우 -> 개인정보 출력
 * 3. 정보수정 -
 * 4. 정보삭제 -
 * 5. 종료
 */

package d20200513.yonghojoon_sql_dao_dto;

public class MainClass {

	public static void main(String[] args) {	//메인 메소드 시작
		
		Menus startProgram = new Menus();	//Menus 클래스 startProgram 객채로 생성 
		startProgram.mainMenu();	//startProgram 객체로 Menus 클래스의 mainMenu() 메소드 호출

	}

}
