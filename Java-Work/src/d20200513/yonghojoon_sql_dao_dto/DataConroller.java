package d20200513.yonghojoon_sql_dao_dto;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class DataConroller {

	Menus menu = new Menus();
	DataAccess dao = new DataAccess();
	
	Scanner sc = new Scanner(System.in);
	
	public boolean checkDuplication(String id) {
		
		boolean idChecker = false;
		
		ArrayList<String> idList = new ArrayList<String>();
				
		idList = dao.CheckIdRequest();
		
		if(idList.contains(id)) {
			idChecker = true;
		} else {
			idChecker = false;
		}

		return idChecker;
	}
	
	public void setInfo() {		//회원 정보 저장 메소드
		DataTransfer dto = new DataTransfer();

		System.out.println();
		System.out.println("============ Sign up ============");
		System.out.print("ID: ");
		dto.setId(sc.next());
		
		if(!checkDuplication(dto.getId())) {	//userId 값이 userMap HashMap에 key로 등록 되어있으면 (true)
			System.out.println("중복 검사 Pass\n");
			System.out.println("=================================");
			System.out.print("Password: ");
			dto.setPassword(sc.next());
			System.out.print("Name: ");
			dto.setName(sc.next());
			System.out.print("Birthday: ");
//			String date = dateFormChanger(sc.next());
			String date = sc.next();
			dto.setBirthday(date);
			System.out.print("Email: ");
			dto.setEmail(sc.next());
			System.out.print("Nickname: ");
			dto.setNickname(sc.next());
			System.out.print("Phone: ");
			dto.setPhone(sc.next());
			System.out.print("Gender(Male=1/Female=2): ");
			dto.setGender(sc.nextInt());
			System.out.print("Open your info public(y/n):");
			if(sc.next().equals("y")) {
				dto.setInfo_agree(1);
			} else {
				dto.setInfo_agree(0);;
			}
			
			System.out.println("=================================\n");
			
			dto.setSignon_date(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
			
			dao.insertSql(dto);
			
		} else {
			System.out.println("중복된 id가 있습니다.\n");
		}
	}

	public String userLogIn() {	//userLogIn 메소드 시작
		
		DataTransfer dto = new DataTransfer();
		
		String input_id, input_password, loggedInId=null, db_gender=null, db_info_agree=null;
		
		System.out.println();
		System.out.println("========== Sign in ==========");
		System.out.print("ID: ");
		input_id = sc.next();			//userId에 문자열을 입력받아 대입
		System.out.print("Password: ");
		input_password = sc.next();		//password에 문자열을 입력받아 대입
		System.out.println("=============================");

		dto = dao.selectSql(input_id);

		if(dto != null)	{
			if(dto.getPassword().equals(input_password)) {
				if(dto.getGender() == 1) {
					db_gender = "M";
				} else {
					db_gender = "F";
				}
				
				if(dto.getInfo_agree() == 1) {
					db_info_agree = "Yes";
				} else {
					db_info_agree = "No";
				}
				
				System.out.println("\n============= Information =============");
	            System.out.println("name:\t\t"+dto.getName());			
	            System.out.println("Birthday:\t"+dto.getBirthday());	
	            System.out.println("Email:\t\t"+dto.getEmail());
	            System.out.println("Nickname:\t"+dto.getNickname());
	            System.out.println("Phone:\t\t"+dto.getPhone());
	            System.out.println("Sign-On date:\t"+dto.getSignon_date());
	            System.out.println("Gender:\t\t"+db_gender);
	            System.out.println("Info public:\t"+db_info_agree);
	            System.out.println("=======================================\n");
	            
	            loggedInId = input_id;
				
			} else {
				System.out.println("Wrong password\n");			//패스워드가 틀렸다고 출력
			}
			
		} else {
			System.out.println("No ID record\n");
		}
		
		return loggedInId;
		
	}

	public boolean updateInfo() {		//정보 수정 메소드 시작
		String record=null, sqlCommand, field=null, input_id=null;					//문자열 변수 userId 선언
		int selectNum = 0;
		
		System.out.print("\n========= Update Info =========");
		input_id = userLogIn();
		
		if(input_id != null) {
				
			System.out.println("=======================================");
			System.out.println("1. Id\n2. Password\n3. Name\n4. Birthday\n5. Email\n6. Nickname\n7. Phone\n8. Gender\n9. Info agreement");
			System.out.println("=======================================");
		
			System.out.print("- 수정할 항목을 선택세요: ");
			selectNum = sc.nextInt();	//정수를 입력받아 selectNum 변수에 저장
			System.out.print("- 수정사항을 입력하세요: ");
			
			switch(selectNum) {		//selectNum이 가지는 번호로 case 이동
			case 1:		//회원가입 
				record = sc.next();
				field = "id";
				System.out.println();
				break;
			case 2:		//로그인
				record = sc.next();
				field = "password";
				System.out.println();
				break;
			case 3:		//정보수정
				record = sc.next();
				field = "name";
				System.out.println();
				break;
			case 4:		//정보삭제
				record = sc.next();
				field = "birthday";
				System.out.println();
				break;
			case 5:		//종료
				record = sc.next();
				field = "email";
				System.out.println();
				break;
			case 6:
				record = sc.next();
				field = "nickname";
				System.out.println();
				break;
			case 7:
				record = sc.next();
				field = "phone";
				System.out.println();
				break;
			case 8:
				record = sc.next();
				field = "gender";
				System.out.println();
				break;
			case 9:
				record = sc.next();
				field = "info_agree";
				System.out.println();
				break;
			default:
				System.out.println("Error! - 1~10번 중에서 선택하세요.\n");
				break;
			}
	
			sqlCommand = "update user set "+field+" = '"+record+"' where id = '"+input_id+"';";
			
			dao.updateSql(sqlCommand);
			
		} else {
			System.out.println("Log-in fail\n");
		}
		
		return true;
	}
	
	public void deleteInfo() {		//정보 삭제 메소드 시작
		String input_id, confirm;					//문자열 변수 userId 선언
		
	
		System.out.print("\n========= Delete Info =========");
		input_id = userLogIn();
		
		if(input_id != null) {
			System.out.print("Delete?(y/n): ");
			confirm = sc.next();
			
			if(confirm.equals("y") || confirm.equals("Y"))
				dao.deleteSql(input_id);
			
		} else {
			System.out.println("삭제 취소\n");
		}
			
	}
    
//	public String dateFormChanger(String inputDate) { //날짜 변경
//		String date=null, testDate = "19990909";//880416, 1988416
//		
//		testDate.length();
//		
//		System.out.println();
//		
//		return date;
//	}
}
