package d20200508.prof;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.*;
import java.util.*;

public class Member_View {	
	ArrayList<Member_data> sr = new ArrayList<>();
	Member_data data = new Member_data();				
	private File file;

	public void add() throws IOException{
		System.out.println("---------텍스트 값을 각 객체에 추가시킨 값---------------");
		try{
			String s;
			String[] array;

			file = new File("person.txt");				

			FileReader filereader = new FileReader(file);
			BufferedReader bos = new BufferedReader(filereader);

			while((s=bos.readLine())!=null){
				array=s.split(",");																							
				data = new Member_data();																				
				data.setName(array[0]);																					
				data.setE_mail(array[1]);
				data.setPhonenumber(array[2]);

				sr.add(data);
			}
		}catch (IOException E10){
			E10.printStackTrace();
		}
		print();
	}


	public void print() {														
		for(int i=0; i<sr.size(); i++) {
			System.out.println(sr.get(i).getName() + "/" + sr.get(i).getE_mail() + "/" + sr.get(i).getPhonenumber());
		}
	}

	public void dataremove() {									
		for(int i=0; i<sr.size(); i++) {											
			for(int j=i+1; j<sr.size(); j++) {									
				if(sr.get(i).getName().equals(sr.get(j).getName())) {		
					sr.remove(sr.get(i));											
				}																	
			}
		}
		System.out.println("-----------------제거한 다음 값-----------------");
		print();
	}

	void sort_interface() {
		System.out.println("--------------------정렬된 값--------------------");
		Collections.sort(sr);
		print();
	}

}