package d20200508.prof;


public class Member_data implements Comparable<Member_data>{

	private String name;												
	private String phonenumber;
	private String e_mail;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhonenumber() {
		return phonenumber;
	}
	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}
	public String getE_mail() {
		return e_mail;
	}
	public void setE_mail(String e_mail) {
		this.e_mail = e_mail;
	}
	@Override
	public int compareTo(Member_data o) {
		return name.compareTo(o.getName());
	}
}