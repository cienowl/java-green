/*
 * 텍스트파일(person.txt)에 저장된 회원 명단을 입력받아 중복을 제거한 후, 정렬하여 출력하는 프로그램을 작성하여 제출하시오
 * - 회원 정보는 성명(영문), 연락처, 이메일주소 항목으로 구성(항목별 자료는 ,로 구분되어있음)
 * 1. 회원 정보를 텍스트파일에서 읽어 들인 후, 중복된 데이터를 제거해야 한다.
 * 2. 중복이 제거된 데이터는 성명을 기준으로 알파벳 순서대로 정렬해야 한다.
 * 3. JAVA API 중 Collection API(List, Map, Set, etc...) 를 이용해서 데이터를 처리한다
 * 4. 비정상적으로 처리되는 작업에 대한 임의의 예외 처리를 추가한다.
 * 5. 작업에 맞게 클래스 및 메소드를 나누어 작성한다.
 */


package d20200508;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

class Database implements Comparable<Database>{
	
	private String name;
	private String tel;
	private String email;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override
	public int compareTo(Database person) {		//Comparable 인스턴스 메소드 재정의, 매개변수 Database 객체형 person
		return this.name.compareTo(person.name);	//private name에 매개변수로 넘어온 객체의 name과 비교하여 리턴 (정렬의 기준 지정)
	}
	
}


class MemberInfo{
	
	ArrayList<Database> list = new ArrayList<Database>();
	
	public MemberInfo() {	//생성자 실행과 함께 파일 읽어오기
		
		FileInputStream fi = null;
		
		try {
			fi = new FileInputStream("D:/javaspring/database/person.txt");		//해당 경로 파일 열기
			byte[] byteData = new byte[fi.available()];		//파일 길이만큼 바이트로 변환하여 byteData 매열에 저장
			
			while(fi.read(byteData) != -1) {	//파일의 끝까지 읽음
			}
			
			String byteIn = new String(byteData);	//읽은 데이터를 문자열형으로 변환하여 byteIn 변수에 저장
			String[] line = byteIn.split("\n");		//읽은 데이터를 줄바꿈 단위로 분리해서 문자열형 배열 line에 저장
			
			
			for(int i = 0; i < line.length; i++) {		//line에 저장된 문자열 갯수만큼 반복
				Database db = new Database();			//모델러 객체 db 생성
			
				String[] infoElement = line[i].split(",");		//저장된 라인을 콤마 단위로 분리하여 infoElement 배열에 저장
//				System.out.println(line[i]);
				
				for(int j = 0; j < infoElement.length; j++) {	//infoElement 배열 길이 만큼 반복
					switch(j) {
					case 0:
						db.setName(infoElement[0]);		//0번 인덱스는 db 객체 name 에 저장
//						System.out.println(infoElement[0]);
						break;
					case 1: 
						db.setTel(infoElement[1]);		//1번 인덱스는 db 객체 tel 에 저장
//						System.out.println(infoElement[1]);
						break;
					case 2: 
						db.setEmail(infoElement[2]);	//2번 인덱스는 db 객체 email 에 저장
//						System.out.println(infoElement[2]);
						break;
					}
				}
				list.add(i,db);		//객체 db를 해당 인덱스 위치 리스트에 저장

			}
			
			controller();	//컨트롤러 메소드 호출

		} catch(FileNotFoundException e) {
			System.out.println(e + " => 파일을 찾을 수 없습니다.");
		} catch(Exception e) {
			System.out.println(e + " => 오류");
		} finally {
			try {
				fi.close();
			} catch(Exception e) {
				System.out.println(e + "파일 종료 오류");
			}
		}
		
	}
	
	public void controller() {	//컨트롤러 메소드 시작
		System.out.println("======== Before Remove Duplication ========");
		printMember();	//출력 메소드 호출
		
		removeDuplication();	//중복제거 메소드 호출
		
		System.out.println("\n======== Before Sort ========");
		printMember();	//출력 메소드 호출

		sortMember();	//정렬 메소드 호출
		
		System.out.println("\n======== After Sort ========");
		printMember();	//출력 메소드 호출
	}
	
	public void removeDuplication() {	//중목제거 메소드 시작

		HashMap<String,Database> hash = new HashMap<String,Database>();	//HashMap 객체 hash 생성
		
		for(int i = 0; i < list.size(); i++)
			hash.put(list.get(i).getName(), list.get(i));	//key값으로 이름, value값으로 list내 객체를 HashMap 에 저장
		
		list = new ArrayList<Database>(hash.values());	//기존 리스트에 hash 값들을 저장

	}
	
	public void sortMember() {		//리스트 정렬 메소드 시작
		Collections.sort(list);		//리스트를 이용해 정렬
	}
	
	public void printMember() {		//리스트 내용 출력 메소드
				
		for(int i = 0; i<list.size(); i++) {	//list 사이즈만큼 반복
			System.out.print("Name: " + list.get(i).getName() + "\tTel: " + list.get(i).getTel() + "\tEmail: " + list.get(i).getEmail() + "\n");	//내용 출력
		}
				
	}	
	
}


public class Examination01 {

	public static void main(String[] args) {
		
		new MemberInfo();	//MemberInfo() 클래스 생성

	}
	

}
