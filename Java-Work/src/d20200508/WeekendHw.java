/*
 * 회원 명단(이름)을 키보드로 입력받아 입력된 자료 중 중복을 제거한 후, 정렬하여 출력하는 프로그램을 작성하여 제출하시오
 * - 회원 명단(이름): John, Chris, John, Alfred, Bach 
 * 1. 회원 정보를 스캔하여 읽어 들인 후, 중복된 데이터를 제거해야 한다.
 * 2. 중복이 제거된 데이터는 성명을 기준으로 알파벳 순서대로 정렬해야 한다.
 * 3. JAVA API 중 Collection API(List, Map, Set, etc...) 를 이용해서 데이터를 처리한다
 * 4. 작업에 맞게 클래스 및 메소드를 나누어 작성한다.
 */

package d20200508;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Scanner;

class Function {
	ArrayList<String> list = new ArrayList<String>();
	
	Scanner sc = new Scanner(System.in);

	public Function() {
		int idx = 0;
		String name = "";
		
		System.out.println("======= Name Collector =======");
		
		while(true) {
			System.out.print("Input names: ");
			name = sc.next();
		
			if(name.equals("quit"))		//quit 입력시 반복문 탈출
				break;
			
			list.add(idx, name);		//list 에 항목 추가
				
			idx++;	//인덱스 번호 카운터		
		}
	}
	
	public void sortDatabase() {
		
		Collections.sort(list);
		
	}
	
	public void printDatabase() {
		
		for(int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}
		
	}

	public void removeDuplication() {

		HashSet<String> hash = new HashSet<String>(list);		//hashSet을 이용한 중복값 제거
		list = new ArrayList<String>(hash);		//hashSet 을 다시 list에 생성
	
	}
	
	public void controller() {
		
		System.out.println("\n===== After Input =====");
		printDatabase();
		
		System.out.println("\n===== Remove Duplication =====");
		removeDuplication();
		printDatabase();
		
		System.out.println("\n===== After Sort =====");
		sortDatabase();
		printDatabase();
		
	}
	
}

public class WeekendHw {

	public static void main(String[] args) {
		
		Function fn = new Function();	//Function 클래스 객체 생성
		fn.controller();	//contoller 메서드 호출
		
	}

}
