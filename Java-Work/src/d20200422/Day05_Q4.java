package d20200422;

//Day05 문제4번
import java.util.Scanner;

class Saram_5_x1 {
	String irum;
	int age;

	Saram_5_x1() {
		irum = "Null";
		age = 0;
	}

	void title() {
		System.out.println("<< 자바의 객체 생성 >>");
		System.out.println("* * * 신상 출력 * * *");
		System.out.println("---------------------");
	}
}

class Day05_Q4 {
	public static void main(String[] args) {
		int sum=0;
		double avg;
		
		Saram_5_x1 k1 = new Saram_5_x1();
		Saram_5_x1 k2 = new Saram_5_x1();
		Saram_5_x1 k3 = new Saram_5_x1();
	
		Scanner userInput = new Scanner(System.in);
		System.out.print("이름을 입력하세요: ");
		k1.irum = userInput.next();
		System.out.print("나이를 입력하세요: ");
		k1.age = userInput.nextInt();

		System.out.print("이름을 입력하세요: ");
		k2.irum = userInput.next();
		System.out.print("나이를 입력하세요: ");
		k2.age = userInput.nextInt();

		System.out.print("이름을 입력하세요: ");
		k3.irum = userInput.next();
		System.out.print("나이를 입력하세요: ");
		k3.age = userInput.nextInt();

		sum = k1.age + k2.age + k3.age;
		avg = sum/3.0;	

		k1.title();
		System.out.println("avg sum == " + k1.age + " + " + k2.age + " + " + k3.age + " == " + sum);
		System.out.println("average age == " + avg);	
	}
}
