package d20200422;

//클래스 선언 : 구성요소(변수) -> private
//				동작(메소드) -> public, 생략
class Exam01_1 {	//틀
	private int value;		//private: 외부접근X
	static int push;		//모든 객체가 공통으로 사용하는 변수(공용변수)

	void dataPrint(int va) {
		value = va;
		System.out.println("value = " + value);
	}

	//생성자(메소드):	메소드명이 클래스명과 같아야함, 객체생성시 자동 호출, 접근제한자: public, 생략
	//생성자는 객체당 한번씩 호출. 반환값 처리X (반환형X), 매개변수 가능
	
	Exam01_1(String a) {
		System.out.println("생성자가 실행 되었습니다.");
	}
	
}

class Example02 {
	public static void main(String[] args) 
	{
		Exam01_1 obj = new Exam01_1("java");	//obj: 객체, -> 생성자 호출 (매개변수 넘길수 있음)
		//obj.value = 123; =		//value변수가 private로 선언되어 있기 때문에 외부 접근X
		obj.dataPrint(123);

		Exam01_1 obj1 = new Exam01_1("web");
		obj1.dataPrint(456);

		Exam01_1.push = 789;	//static: 정적, 클래스명.static변수명 = 값;
		//static요소는 객체명.static요소로 처리 불가능
	}
}
