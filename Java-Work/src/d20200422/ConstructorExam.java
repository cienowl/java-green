package d20200422;

class Puppy {
	String str;
	int i;

	public Puppy() {	//생성자
		System.out.println("생성자 public Puppy() 호출되었습니다.");
		this.printPuppyName();
		this.str = "메리";
		this.i = -98998;
	}

	public Puppy(int a) {

	}

	public void printPuppyName() {
		System.out.println("printPuppyName() 호출되었습니다.");
		System.out.println("변수 str = " + str + ", i = " + i);
	}
}

class ConstructorExam {
	public static void main(String[] args) {
		Puppy p = new Puppy();		//객체 p 생성
		p.printPuppyName();
		Puppy p1 = new Puppy(789);
	
	
	}
}
