package d20200422;

//클래스 선언 : 구성요소(변수) -> private
//				동작(메소드) -> public, 생략
class Example01 {	//틀
	private int value;		//private: 외부접근X
	static int push;		//모든 객체가 공통으로 사용하는 변수(공용변수)

	void dataPrint(int va) {
		value = va;
		System.out.println("value = " + value);
	}
	//생성자(메소드): 메소드명이 클래스명과 같아야함
	Example01() {
		System.out.println("생성자가 실행 되었습니다.");
	}

}


class Exam02 {
	public static void main(String[] args) 
	{
		Example01 obj = new Example01();	//obj: 객체
		//obj.value = 123; =		//value변수가 private로 선언되어 있기 때문에 외부 접근X
		obj.dataPrint(123);

		Example01 obj1 = new Example01();
		obj1.dataPrint(456);

		Example01.push = 789;	//static: 정적, 클래스명.static변수명 = 값;
		//static요소는 객체명.static요소로 처리 불가능
	}
}
