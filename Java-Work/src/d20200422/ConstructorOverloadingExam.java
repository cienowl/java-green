package d20200422;

class ConstructorExample {
	public ConstructorExample() {
		System.out.println("생성자 실행: Null");
	}
	
	public ConstructorExample(String stdLine) {
		System.out.println("생성자 실행: " + stdLine);
	}
	
	public ConstructorExample(String language, String greetings) {
		System.out.println("생성자 실행: " + language + ", " + greetings);
	}
	
	public ConstructorExample(char spell) {
		System.out.println("생성자 실행: " + spell);
	}
	
	public ConstructorExample(boolean flag) {
		System.out.println("생성자 실행: " + flag);
	}
}

class ConstructorOverloadingExam {
	public static void main(String[] args) {
		ConstructorExample p1 = new ConstructorExample();
		System.out.println("======= 1번 완료 =======");
		
		ConstructorExample p2 = new ConstructorExample("A");
		System.out.println("======= 2번 완료 =======");
		
		ConstructorExample p3 = new ConstructorExample("JAVA","Hi");
		System.out.println("======= 3번 완료 =======");
		
		ConstructorExample p4 = new ConstructorExample('F');
		System.out.println("======= 4번 완료 =======");
		
		ConstructorExample p5 = new ConstructorExample(true);
		System.out.println("======= 5번 완료 =======");
	}
}
