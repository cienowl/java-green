package d20200422;

class ObjectOne {
	int a;

	public ObjectOne(int i) {
		System.out.print(" [===생성자 실행===] ");
	}
}  

class ObjectInArray {
	public static void main(String[] args) 
	{
		ObjectOne[] obj = new ObjectOne[5];		//객체 배열생성. 현시점에서 new는 배열을 생성하는 new
		System.out.println("obj = " + obj);

		for(int i = 0; i<obj.length; i++) {
			System.out.print("생성전: obj[" + i + "] = " + obj[i]);
			obj[i] = new ObjectOne(10);			//따라서 생성자 overloading을 위해서 여기서 값을 넣음
			System.out.println(", 생성후 : obj[" + i + "] = " + obj[i]);
		}
	}
}
