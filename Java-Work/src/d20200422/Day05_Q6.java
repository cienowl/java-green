package d20200422;

//Day05 문제6번
import java.util.Scanner;

class Sungjuk {
	int javaScore, cobolScore, fortranScore, total;	
	double avg;
	String name;
	String hakbun;

	public void setValue() {					//setValue 메서드
		Scanner sc = new Scanner(System.in);	//sc 스캐너 객체 생성
		System.out.println("Input student's information");
		System.out.print("Name: ");
		name = sc.next();
		System.out.print("Hakbun: ");
		hakbun = sc.next();
		System.out.print("Java: ");
		javaScore = sc.nextInt();
		System.out.print("Cobol: ");
		cobolScore = sc.nextInt();
		System.out.print("Fortran: ");
		fortranScore = sc.nextInt();
	}

	public void calcValue() {		//calcValue 메서드
		total = javaScore + cobolScore + fortranScore;	//각 점수 합 계산 후 total에 저장
		avg = total/3.0;		//평균 계산후 저장
	}
}

class Day05_Q6 {
	public static void main(String[] args) {
		int i, j;								//for문에 사용할 변수

		Sungjuk student[] = new Sungjuk[5];		//객체 배열선언 [5]
		for(i=0; i<student.length; i++) {		//객체 길이만큼 반복
			student[i] = new Sungjuk();			//객체 생성
			student[i].setValue();				//각 객체의 setValue 메서드 실행
			student[i].calcValue();				//각 객체의 점수 총점, 평균 계산 메서드 실행
		}
		
		System.out.println("\n** Sungjukpyo <before ascending sort> **");
		System.out.println("===========================================================");
		System.out.println("hakbun	name	java	cobol	fortran	total	avg");
		System.out.println("===========================================================");
		for(i = 0; i<student.length; i++) {		//객체배열 길이만큼 반복하면서 내용 출력
//			System.out.println(student[i].hakbun + "\t" + student[i].name + "\t" + student[i].javaScore  + "\t" + student[i].cobolScore  + "\t" + student[i].fortranScore  + "\t" + student[i].total  + "\t" + student[i].avg);
			System.out.printf("%s\t%s\t%d\t%d\t%d\t%d\t%.2f\n", student[i].hakbun, student[i].name, student[i].javaScore, student[i].cobolScore, student[i].fortranScore, student[i].total, student[i].avg);
		}
		
		Sungjuk temp;		//객체주소 swap하기 위해 임시로 객체주소 변수선언
		for(j=0; j<student.length; j++) {
			for(i=j; i<student.length; i++) {
				if(student[j].total > student[i].total) {		//각 객체 total 점수 기준으로 앞쪽 total 점수가 높으면
					temp = student[j];							//높은점수를 가진 객체 주소를 임시주소에 복사
					student[j] = student[i];					//낮은점수를 가진 객체 주소를 앞으로 이동
					student[i] = temp;							//임시주소 값을 복사
				}
			}
		}
		
		System.out.println("\n** Sungjukpyo <after ascending sort> **");
		System.out.println("============================================================");
		System.out.println("hakbun	name	java	cobol	fortran	total	avg");
		System.out.println("============================================================");
		for(i=0; i<student.length; i++) {
//			System.out.println(student[i].hakbun + "\t" + student[i].name + "\t" + student[i].javaScore  + "\t" + student[i].cobolScore  + "\t" + student[i].fortranScore  + "\t" + student[i].total  + "\t" + student[i].avg);
			System.out.printf("%s\t%s\t%d\t%d\t%d\t%d\t%.2f\n", student[i].hakbun, student[i].name, student[i].javaScore, student[i].cobolScore, student[i].fortranScore, student[i].total, student[i].avg);
		}
	}
}
