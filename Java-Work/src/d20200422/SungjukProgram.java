package d20200422;



import java.util.InputMismatchException;
import java.util.Scanner;

class SungjukProgramFunction {
	
	Scanner scInfo = new Scanner(System.in);
	
	String name;
	String hakbun;
	private int htmlScore, jspScore, springScore, totalScore;
	double avgScore;

	void SungjuckProgramFunction() {
		totalScore = 0;
		avgScore = 0;
	}

	public boolean inputSungjuk(int personNum) {

		System.out.println("======== " + personNum + "번학생 정보를 입력하세요. ========");
		System.out.print("이름: ");
		name = scInfo.next();
		System.out.print("학번: ");
		hakbun = scInfo.next();
		System.out.print("html 점수: ");
		htmlScore = scInfo.nextInt();
		System.out.print("jsp 점수: ");
		jspScore = scInfo.nextInt();
		System.out.print("spring 점수: ");
		springScore = scInfo.nextInt();

		System.out.println("==== " + personNum + "번학생 성적 입력이 완료되었습니다. ====\n");

		return true;

	}

	public boolean calcSungjuk() {

		totalScore = htmlScore + jspScore + springScore;
		avgScore = totalScore / 3.0;

		return true;

	}

	public void printSungjukLine() {

		System.out.printf("%s\t%s\t%d\t%d\t%d\t%d\t%.2f\n", name, hakbun, htmlScore, jspScore, springScore, totalScore, avgScore);

	}
	
}

public class SungjukProgram {

	public static void main(String[] args) {

		String errMsg = "Error! - 성적이 입력되지 않았습니다.";		//Error 메세지 출력을 위해 문자열 저장
		int selecNum = 0, i = 0;			//정수형 변수 selecNum, i 선언 후 0으로 초기화
		boolean checkInfo = false;			//정보가 저장되었는지 확인 하기 위한 변수

		SungjukProgramFunction[] sj = new SungjukProgramFunction[3];		//
		for (i = 0; i < sj.length; i++) {
			sj[i] = new SungjukProgramFunction();
		}

		Scanner scNum = new Scanner(System.in);
		
		while (selecNum != 4) {

			System.out.println("***** 성적처리 프로그램 Ver.0.0.1 *****\n1. 입력\n2. 계산\n3. 출력\n4. 종료");
			System.out.print("- 당신이 원하는 메뉴를 선택세요: ");

			try {
				selecNum = scNum.nextInt();
			} catch (InputMismatchException e) {
				System.out.println("Error! - 정수로 입력하세요. ");
				selecNum = 4;
			}

			System.out.println();

			switch (selecNum) {

			case 1:
				for (i = 0; i < sj.length; i++)
					checkInfo = sj[i].inputSungjuk((i + 1));
				break;
			case 2:
				if (checkInfo) {
					for (i = 0; i < sj.length; i++)
						sj[i].calcSungjuk();
					System.out.println("***** 성적 계산이 완료 되었습니다. *****\n");
				} else {
					System.out.println(errMsg + "\n");
				}
				break;
			case 3:
				if (checkInfo) {
					System.out.println("==================== Sungjuk Output ====================");
					System.out.println("Name\tHakbun\tHTML\tJSP\tSpring\tTotal\tAvg");
					System.out.println("--------------------------------------------------------");

					for (i = 0; i < sj.length; i++)
						sj[i].printSungjukLine();

					System.out.println("========================================================\n");
				} else {
					System.out.println(errMsg + "\n");
				}
				break;
			case 4:
				System.out.println("프로그램이 종료되었습니다.");
				scNum.close();
				break;
			default:
				System.out.println("Error! - 1~4번 중에서 선택하세요.\n");
				break;

			}

		}

	}

}
