package d20200422;

//Day05 문제5번
import java.util.Scanner;

class Saram_5_x2 {
	String irum;
	int age;

	Saram_5_x2() {
		irum = "Null";
		age = 0;
	}

	void title() {
		System.out.println("<< 자바의 객체 생성<배열 이용> >>");
		System.out.println("* * * 신상 출력 * * *");
		System.out.println("---------------------");
	}
}

class Day05_Q5 {
	public static void main(String[] args) {
		int sum=0, i;
		double avg;

		Scanner userInput = new Scanner(System.in);

		Saram_5_x2 k[] = new Saram_5_x2[3];
		
		for(i = 0; i<k.length; i++) {
			k[i] = new Saram_5_x2();
			
			System.out.print("이름을 입력하세요: ");
			k[i].irum = userInput.next();
			System.out.print("나이를 입력하세요: ");
			k[i].age = userInput.nextInt();

			sum += k[i].age;
		}
		userInput.close();
		avg = sum/3.0;	

		k[0].title();
		System.out.println("avg sum == " + k[0].age + " + " + k[1].age + " + " + k[2].age + " == " + sum);
		System.out.println("average age == " + avg);	
	}
}
