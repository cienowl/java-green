package d20200417;

class Day02_Q1 {
	public static void main(String[] args)	{

		int total = 0;	// 합을 구하기 위한 변수

		for(int i=5; i<88; i++){	//5부터 87까지
			total += i;				//전체 총합
		}
		
		System.out.println("sum(5 to 87)" + total);	//출럭
	}
}
