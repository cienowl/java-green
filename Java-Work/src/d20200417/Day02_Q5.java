package d20200417;

class Day02_Q5 {
	public static void main(String[] args) {

		for(int i=2; i<=8; i=i+2) {				//2단부터 8단까지 짝수 단
			for(int j=1; j<=i; j++) {			//해당 단수까지만 곱
				System.out.println(i + " * " + j + " = " + (i*j));	// 구구단 출력문
			}
			System.out.println();	// 구구단수 구분을 위한 줄바꿈
		}
	}
}
