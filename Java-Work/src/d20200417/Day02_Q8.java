package d20200417;

import java.util.Scanner;

public class Day02_Q8 {

	public static void main(String[] args) {
		
		int alpha, omega;		//시작점, 끝점 변수
		
		Scanner inputNum = new Scanner(System.in);		//입력 객체 생성
		
		while(true) {			//반복문 시작
			System.out.print("Input two numbers: ");
			alpha = inputNum.nextInt();					//alpha 변수에 시작점 저장
			omega = inputNum.nextInt();					//omega 변수에 끝점 저장
			
			if(alpha > omega || (alpha<1 || 9<alpha) || (omega<1 || 9<omega)) {	//끝점이 시작점보다 작거나 시작점과 끝점이 1~9 사이가 아닌경우
				System.out.println("First input number must be less than second number, and the numbers are in range 1~9.");		//에러문 출력
			} else {
				break;		//반복문 탈출
			}
		}
			
		inputNum.close();	//입력 객체 종료
		
		for(int i=alpha; i<=omega; i++) {	//시작점부터 끝점까지 반복
			for(int j=1; j<=9; j++) {	//1에서 9까지 반복
				System.out.println(i + " * " + j + " = " + (i*j));	//단수 출력
			}
			System.out.println();	//줄바꿈
		}	

	}

}
