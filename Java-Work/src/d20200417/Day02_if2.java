package d20200417;

class Day02_if2 {
	public static void main(String[] args) { //main: 프로그램시작

		int su = (int)(Math.random()*10);	//su값이 짝수인지 홀수인지 판단

		if(su%2 == 1) {
			System.out.println(su + "홀수입니다.");
		} else {
			System.out.println(su + "짝수입니다.");
		}		
	}
}
