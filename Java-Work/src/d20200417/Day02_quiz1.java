package d20200417;

class Day02_quiz1 {
	public static void main(String[] args) {
		
		int korScore = (int)(Math.random()*101);
		int engScore = (int)(Math.random()*101);
		int matScore = (int)(Math.random()*101);
		int totScore = korScore + engScore + matScore;
		double avgScore = totScore/3.0; //double형 자료는 equals(==) 식 사용x

		if(avgScore >= 90) {
			System.out.println(avgScore + "A학점");
		} else if(avgScore >= 80) {
			System.out.println(avgScore + "B학점");
		} else if(avgScore >= 70) {
			System.out.println(avgScore + "C학점");
		} else {
			System.out.println(avgScore + "F학점");
		}
	}
}
