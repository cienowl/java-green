package d20200417;

class Day02_Q2 {
	public static void main(String[] args) {

		int total = 0;		//합을 위한 변수

		for(int i=1; i<=100; i++) {		//1부터 100까지 loop
//			if(i%2==0 && i%7==0) {
			if(i%14==0) {				//2와 7의 최소공배수 14로 나누어지는 수 일때
				System.out.println("2 & 7 common multiple -> " + i);		// 출력
				total += i;				//모두 더함
			}
		}

		System.out.println("sum -> " + total);	//총합 출력
	}
}
