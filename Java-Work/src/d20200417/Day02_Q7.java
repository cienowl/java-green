package d20200417;

import java.util.Scanner;

public class Day02_Q7 {
	public static void main(String[] args) {
		
		int korScore=0, engScore=0, matScore=0, totScore;	//한국어, 영어, 수학 점수, 총점 변수 선언
		double avgScore;									//평균 계산을 위한 double 형 변수 선언
		char letterGrade='F';								//학점 저장을 위한 변수
		
		Scanner inputScore = new Scanner(System.in);		//inputScore 스캐너 객체 생성
		
		while(true) {										//반복문 시작
			System.out.print("Input Kor score: ");
			korScore = inputScore.nextInt();				//한국어 점수 입력
			System.out.print("Input Eng score: ");
			engScore = inputScore.nextInt();				//영어 점수 입력
			System.out.print("Input Mat score: ");
			matScore = inputScore.nextInt();				//수학 점수 입력
			
			if(korScore < 50 || engScore < 50 || matScore < 50) {	//각과목 점수 50 이하 입력이면 에러문 출력, 재입력 받음
				System.out.println("Each score must be higher than 50");	//에러문
			} else {
				break;		//입력된 모든 점수가 50점 이상이면 반복문 탈출
			}
		}
			
		inputScore.close();		//input scanner 객체 종료
		
		totScore = korScore + engScore + matScore;		//총점 계산 후 저장
		avgScore = totScore/3.0;						//총점을 이용해 평균 계산

		System.out.println("Total Score: " + totScore);		//총점 출력
		System.out.printf("Avg Score: %.2f", avgScore);		//평균 출력
		System.out.println();		//줄바꿈
		
		if(avgScore >= 90) {			//평균이 90점 이상이면
			letterGrade = 'A';	//"A" 학점 저장
		} else if(avgScore >= 80) {		//평균이 80점 이상이면
			letterGrade = 'B';	//"B" 학점 저장
		} else {						//그 외
			letterGrade = 'F';	//"F" 학점 저장
		}
		System.out.println(letterGrade + " grade");	//학점 출력
	}
}
