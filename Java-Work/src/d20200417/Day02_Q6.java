package d20200417;

class Day02_Q6 {
	public static void main(String[] args) {

		int i = 1;
		int num = 1;
		int temp = 1;

		while(true){
			
			if(i<=100){			// 100보다 작은 동안
				num = i;		// 출력을 위해 숫자 이동
				System.out.println(num);	//100까지는 순서대로 숫자 출력
			} else {			//100이 넘는 시점부터
				num = i-temp;	//그 수에서 temp 값 만큼 빼고 저장 101-1, 102-3, 103-5, ...
				System.out.println(num);		//한 값을 출력
				temp += 2;		//temp를 2씩 증가시키면서 뺌 1, 3, 5, 7, 9, ...
				if(num == 1) break;
			}

			i++;		//increment
		}
	}
}
