package d20200417;

class Day02_Q3 {
	public static void main(String[] args) {

		int total = 1;		//곱셈을 위해 초기화 1로

		for(int i=1; i<=10; i++){		//1부터 10까지 loop
			total *= i;					//기존 값에 다음값 곱함
		}

		System.out.println("1~10 multiplication -> " + total);		//결과 값 출력
	}
}
