package d20200417;

class Day02_Q4 {
	public static void main(String[] args) {
		int sum = 0;			// 총합을 위한 변수
		int step = 1;			// increment를 위한 변수

		while(sum<1000) {		//sum 값이 1000을 넘으면 종료
			if(step%2==1 || step%6==0)	//조건식
				sum += step;			//총합
			step++;					//스텝 increment
		}
		System.out.println(step + " times, sum = "  + sum);		//출력
	}
}
