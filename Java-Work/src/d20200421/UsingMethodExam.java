package d20200421;

/*
Method이름: methodExam01
ReturnType: 정수
Parameter: 정수 1개
하는 일: methodExam01이 호출되었습니다. 출력
		인수로 받은 정수 출력, 정수*2 리턴

Method이름: methodExam02
ReturnType: String
Parameter: String 1개, 정수 1개
하는 일: methodExam02가 호출되었습니다. 출력,
		인수로 받은 String 에 바보를 붙여서 출력, 
		인수로 받은 String 에 바보를 붙여서 리턴

Method이름: methodExam03
ReturnType: 리턴안함
Parameter: 정수 1개와 String 1개
하는 일: methodExam03이 호출되었습니다. 출력

Method이름: methodExam04
ReturnType: 실수
Parameter: 첫번째 인수(정수), 두번째 인수(실수), 세번째인수(정수)
하는 일: methodExam04가 호출되었습니다. 출력
		실수값 리턴
*/

class UsingMethodExam {		//Class 시작

	public int methodExam01(int numIn) {		//매개변수 int:1개
		System.out.println("methodExam01이 호출 되었습니다.");	//출력
		System.out.println(numIn);		//매개변수 내용 출력
		
		return numIn*2;		//리턴 int형
	}

	public String methodExam02(String stn, int numIn) {			//매개변수 String: 1개, int: 1개, String 반환
		System.out.println("methodExam02가 호출 되었습니다.");	//출력문
		System.out.println(stn + " 바보");						//바보 붙여서 출력

		return stn + " 바보";		//바보 붙여서 String형 리턴
	}

	void methodExam03(int numIn, String strIn) {		//매개변수 int: 1개, String: 1개, 리턴없음
		System.out.println("methodExam03이 호출 되었습니다.");	//출력문
	}

	public double methodExam04(int numIn1, double numIn2, int numIn3) {	//매개변수 정수:2개, 실수:1개
		System.out.println("methodExam04가 호출 되었습니다.");	//출력문

		return numIn2;		//double형 리턴
	}

	public static void main(String[] args) {
		int numFwd = 2, numRcv;
		double silFwd = 2.1, silRcv;
		String strFwd = "Yeah!";
		String strRcv;

		UsingMethodExam mCall = new UsingMethodExam();

		numRcv = mCall.methodExam01(numFwd);
		System.out.println(" " + numRcv);
		strRcv = mCall.methodExam02(strFwd, numFwd);
		System.out.println(" " + strRcv);
		mCall.methodExam03(numFwd, strFwd);
		silRcv = mCall.methodExam04(numFwd, silFwd, numFwd);
		System.out.println(" " + silRcv);
	}
}
