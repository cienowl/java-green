package d20200421;

class Account {		//객체를 만들어 내는 틀, 속성(변수)과 동작(메소드)으로 이루워짐.
	String name;
	String no;
	long balance;
	void printShow() {		//private 접근제한자
		System.out.println("  계좌명의 : " + name);
		System.out.println("  계좌번호 : " + no);
		System.out.println("  예금잔고 : " + balance);
	}
}

class AccountTester {
	public static void main(String[] args) {
		Account chul = new Account();
		Account young = new Account();

//		chul.name = "철수";
//		chul.no = "123456";
//		chul.balance = 1000;

		young.name = "영희";
		young.no = "654321";
		young.balance = 200;

		chul.balance -= 200;
		young.balance += 100;

		chul.printShow();
		young.printShow();
	}
}
