package d20200421;

class MakeMethodExam {
	public int methodExam01(int a, int b) {
		int sum = a + b;

		System.out.println("methodExam01");
		System.out.println(a + " + " + b + " = " + sum);

		return sum;
	}

	public String methodExam02(String name) {
		System.out.println("methodExam02 " + name);
		return "methodExam02 " + name;
	}

	void methodExam03(int a, String name) {
		System.out.println("methodExam03 " + a);
		System.out.println(name);
	}

	public double methodExam04(int a, double b, int c) {
		System.out.println("methodExam04");
		System.out.println(a + " " + b + " " + c);
		return a+b+c;
	}

	public static void main(String[] args) {
	
		MakeMethodExam mme = new MakeMethodExam();

		int k = mme.methodExam01(33,22);
		System.out.println(k);
		
		String s = mme.methodExam02("java");
		System.out.println(s);
		
		mme.methodExam03(42, "green");
		
		double d = mme.methodExam04(7, 5.5, 8);
		System.out.println(d);
	}
}
