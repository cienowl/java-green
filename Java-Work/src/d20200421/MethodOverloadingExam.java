package d20200421;

class MethodOverloadingExam {
	public int setOverload(int a) {
		System.out.println(a);
		return a;
	}

	public String setOverload() {
		String st = "setOver load String";
		return st;
	}
	
	public boolean setOverload(boolean bool) {
		System.out.println(bool);
		return bool;
	}

	public String setOverload(String st1, String st2) {
		System.out.println(st1);
		System.out.println(st2);

		return st1 + st2;		
	}
	
	public void getOverload(){
		System.out.println("getOverlad void method");
	}

	public char getOverload(String greetingSt) {
		char firstChar = 'a';
		System.out.println(greetingSt);

		return firstChar;
	}

	public String getOverload(String a, String b) {
		System.out.println(a);
		System.out.println(b);
		
		return a + b;
	}

	public float getOverload(float silsu) {
		System.out.println(silsu);

		return silsu;
	}


	public static void main(String[] args) {
		MethodOverloadingExam om = new MethodOverloadingExam();

		om.setOverload(5);
		String re = om.setOverload();
		om.setOverload(false);
		om.setOverload("A", "Java");
		
		om.getOverload();
		char ch = om.getOverload("hello");
		om.getOverload("aa", "BB");
		om.getOverload(5.2f);
		System.out.println("===================");
	}
}
