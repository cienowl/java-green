package d20200421;

/*
* 메소드(Method, function, 기능, 함수)
- 프로그램 기능(단위)
- 선언(정의), 호출

형식)	반환형 메소드명( 매개변수, ...) {
			실행문장;
			return 반환값;
		}

*/

class Day04_Method {
	
	void method01(int su, int su1) {
		System.out.println(" method01!");
		System.out.println("변수 value = " + su + su1);
	}

	int method02() {	//이름: method, 매개변수X
		int va = 456;
		System.out.println(" method02!");
		System.out.println(" Variable va = " + va);

		return va;
	}
			
	public static void main(String[] args) {	//main이라는 이름을 가진 메소드
		int value = 123;	//지역 변수

		System.out.println("Start Program");
		System.out.println("변수 value = " + value);
		
		Day04_Method name = new Day04_Method();
		name.method01(value, 456);		//메소드 호출

		System.out.println("Print String after method call");

		name.method02();
		//System.out.println(" 변수 va = " + re);
	}

	
}
