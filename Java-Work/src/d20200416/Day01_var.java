package d20200416;

class Day01_var
{
	public static void main(String[] args) 
	{
		//이름: green
		//이름을 저장하는 변수(공간) 선언
		String name = "green";
		char ch = 'A';
		int i = 12345;
		double d = 12345.623;

		System.out.println("이름: green");
		System.out.println("이름: green");
		System.out.println(name);

		//학생 성적 관리 프로그램
		//이름, 국/영/수, 총점, 평균, 학점
		String student_name = "John";	//학생이름
		int score_korean = 80;			//국어점수
		int score_english = 70;			//영어점수
		int score_math = 100;			//수학점수
		int score_total = score_korean + score_english + score_math;	//총점
		double score_average = score_total/3.0;		//평균
		char lettergrade = 'A';				//학점

		System.out.println("score_korean");
		System.out.println(score_korean);

		//국어줌수 : 90
		System.out.println("이름 : " + student_name);
		System.out.println("국어점수 : " + score_korean);
		System.out.println("영어점수 : " + score_english);
		System.out.println("수학점수 : " + score_math);
		System.out.println("총점 : " + score_total);
		System.out.println("평균 : " + score_average);
		System.out.println("학점 : " + lettergrade);
	}
}


/**
1. 변수:
- data(값, 정보, 자료)를 저장하는 공간
- 자료형: 자료의 size
	char:문자 < int:정수 < double:실수 < String:문자열
- 변수 선언
	형식) 자료형 변수이름;




*/

