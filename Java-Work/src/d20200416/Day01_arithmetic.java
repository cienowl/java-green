package d20200416;

class Day01_arithmetic 
{
	public static void main(String[] args) 
	{
		int i = 5;
		int j = 2;

		System.out.println("i+j=" + (i+j));
		System.out.println("i-j=" + (i-j));
		System.out.println("i*j=" + (i*j));
		System.out.println("i/j=" + (i/j));
		System.out.println("i%j=" + (i%j));
		System.out.println("i=" + i);

		int x = 19 + 4 * 3 / 2 - 10 * 2 + 4;
		System.out.println("19+4*3/2-10*2+4=" + x);
		System.out.println("===============================");

		int a = 5;
		System.out.println(a++);
		System.out.println(a);

		System.out.println(--a);
		System.out.println(a);
		System.out.println("===============================");
		System.out.println(a++ + "," + a + "," + ++a);
		System.out.println("===============================");

		a*=5;

		System.out.println(a);

	}
}
