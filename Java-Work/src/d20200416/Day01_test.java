package d20200416;

class Day01_test 
{
	public static void main(String[] args) 
	{
		/*
		//직원 관리 프로그램
		//이름, 아이디, 비밀번호, 주소, 급여, 성과급(%), 인사평가
		//임의의 값을 지정하시고 예쁘게 출력하시오.

		String name = "김철수";		//이름
		String id = "angel1004";	//아이디 저장
		String pw = "232323";		//비밀번호 저장
		String address = "도로명 주소 도시 국가";	//주소 저장
		String team_name = "영업팀";		//팀명 저장
		int income = 4000;			//급여
		double bonus = 5.2;			//성과급율
		char grade = 'A';			//인사평가 등급
		
		System.out.println("이름 : " + name);
		System.out.println("아이디 : " + id);
		System.out.println("비밀번호 : " + pw);
		System.out.println("주소 : " + address);
		System.out.println("부서 : " + team_name);
		System.out.println("급여 : " + income + "만원");
		System.out.println("성과급 : " + bonus + "%");
		System.out.println("인사평가 : " + grade);
		*/

		int number1 = 100;
		int number2 = 200;
		double number3 = 10;
		double div = (number1 + number2)/7.0;	//연산시 결과는 자료형이 큰 쪽으로..
		int result = (number1 + number2)/(int)7.0;

		System.out.println("나눈 값 : " + div);	//실수 출력
		System.out.println("나눈 값 : " + result);		//실수형을 정수형으로 강제형변환하여 출력
		System.out.println("나머지 : " + (int)number3%3);	//나머지 연산을 사용할 시 반드시 정수타입만
	}
}
