package d20200416;

class Day01_comparison
{
	public static void main(String[] args) 
	{
		int i = 5;
		int j = 3;

		System.out.println("int i = " + i + "\nint j = " + j);
		System.out.println("i >= i ==>> " + (i>=j));
		System.out.println("i > j ==>> " + (i>j));
		System.out.println("i <= i ==>> " + (i<=j));
		System.out.println("i < j ==>> " + (i<j));
		System.out.println("i == j ==>> " + (i==j));
		System.out.println("i != j ==>> " + (i!=j));

		boolean t = true;
		boolean f = false;
		
		System.out.println("true && false = " + (t&&f));
		System.out.println("true && true = " + (t&&t));
		System.out.println("false && false = " + (f&&f));
		System.out.println();

		System.out.println("true || false = " + (t||f));
		System.out.println("true || ture = " + (t||t));
		System.out.println("false || false = " + (f||f));
		System.out.println();

	}
}
