package d20200512.yonghojoon_sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class ProcessSql {
	Menus menu = new Menus();
	
	Connection conn = null;
	Statement stmt = null;
	
	Scanner sc = new Scanner(System.in);
	
	public boolean checkDuplication(String id) {
		boolean checker = false;
		
		ArrayList<String> idList = new ArrayList<String>();
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost/yonghojoon", "root", "sjrnfl12^^");
			System.out.println("연결 성공");
			stmt = conn.createStatement();
			
			String sqlSelect = "select id from user;";
			
			ResultSet rs = stmt.executeQuery(sqlSelect);
			
			int idx = 0;
			while(rs.next()) {
				idList.add(idx, rs.getString(1));
				idx++;
			}
			
			if(idList.contains(id)) {
				checker = true;
			} else {
				checker = false;
			}

		
		} catch(ClassNotFoundException e) {
			System.out.println("드라이버 로딩 실패");
		} catch(SQLException e) {
			System.out.println("에러" + e);
		} finally {
			try {
				if(conn != null) {
					conn.close();
				}
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
		return checker;
	}
	
	public void setInfo() {		//회원 정보 저장 메소드
		
		String id, password, name, birthday, email, nickname, phone, signon_date, agree;
		int info_agree, gender;
		
		System.out.println();
		System.out.println("============ Sign up ============");
		System.out.print("ID: ");
		id = sc.next();
		
		if(!checkDuplication(id)) {	//userId 값이 userMap HashMap에 key로 등록 되어있으면 (true)
			System.out.println("중복 검사 Pass\n");
			System.out.println("=================================");
			System.out.print("Password: ");
			password = sc.next();	//문자열 입력받아서 input객체를 이용해 setPassword 메소드 호출
			System.out.print("Name: ");
			name = sc.next();		//문자열 입력받아서 input객체를 이용해 setName 메소드 호출
			System.out.print("Birthday(yyyy-mm-dd): ");
			birthday = sc.next();	//문자열 입력받아서 input객체를 이용해 setBirthDate 메소드 호출
			System.out.print("Email: ");
			email = sc.next();		//문자열 입력받아서 input객체를 이용해 setSex 메소드 호출
			System.out.print("Nickname: ");
			nickname = sc.next();
			System.out.print("Phone: ");
			phone = sc.next();
			System.out.print("Gender(Male=1/Female=2): ");
			gender = sc.nextInt();
			System.out.print("Open your info public(y/n):");
			agree = sc.next(); 
			
			signon_date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		
			if(agree.equals("y")) {
				info_agree = 1;
			} else {
				info_agree = 2;
			}
			
			System.out.println("=================================\n");
			
			try {
				Class.forName("com.mysql.jdbc.Driver");
				conn = DriverManager.getConnection("jdbc:mysql://localhost/yonghojoon", "root", "sjrnfl12^^");
				System.out.println("연결 성공");
				stmt = conn.createStatement();
				
				String sqlInsert = "insert into user values('"+id+"','"+password+"','"+name+"','"+birthday+"','"+email+"','"+nickname+"','"+phone+"','"+signon_date+"',"+gender+","+info_agree+")";
				stmt.executeUpdate(sqlInsert);
					
			} catch(ClassNotFoundException e) {
				System.out.println("드라이버 로딩 실패");
			} catch(SQLException e) {
				System.out.println("에러" + e);
			} finally {
				try {
					if(conn != null) {
						conn.close();
					}
				} catch(SQLException e) {
					e.printStackTrace();
				}
			}
			
		} else {
			System.out.println("중복된 id가 있습니다.\n");
		}
	}

	public String userLogIn() {	//userLogIn 메소드 시작
		
		String input_id, input_password, sendId=null;			//문자열 변수 password 선언
		String db_id=null, db_password=null, db_name=null, db_birthday=null, db_email=null, db_nickname=null, db_phone=null, db_signon_date=null, db_gender=null, db_info_agree=null;
		int db_gender_int=0, db_info_agree_int=0;
		
		System.out.println();
		System.out.println("========== Sign in ==========");
		System.out.print("ID: ");
		input_id = sc.next();			//userId에 문자열을 입력받아 대입
		System.out.print("Password: ");
		input_password = sc.next();		//password에 문자열을 입력받아 대입
		System.out.println("=============================");
	
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost/yonghojoon", "root", "sjrnfl12^^");
			System.out.println("연결 성공");
			stmt = conn.createStatement();
		
			String sqlSelect = "select * from user where id='"+input_id+"';";
			
			ResultSet rs = stmt.executeQuery(sqlSelect);
			
			if(rs.next()) {
						
				db_id = rs.getString(1);
				db_password = rs.getString(2);
				db_name = rs.getString(3);
				db_birthday = rs.getString(4);
				db_email = rs.getString(5);
				db_nickname = rs.getString(6);
				db_phone = rs.getString(7);
				db_signon_date = rs.getString(8);
				db_gender_int = rs.getInt(9);
				db_info_agree_int = rs.getInt(10);
				
				if(db_password.equals(input_password)) {
					if(db_gender_int == 1) {
						db_gender = "M";
					} else {
						db_gender = "F";
					}
					
					if(db_info_agree_int == 1) {
						db_info_agree = "Yes";
					} else {
						db_info_agree = "No";
					}
					
					System.out.println("\n============= Information =============");
		            System.out.println("name:\t\t"+db_name);			
		            System.out.println("Birthday:\t"+db_birthday);	
		            System.out.println("Email:\t\t"+db_email);
		            System.out.println("Nickname:\t"+db_nickname);
		            System.out.println("Phone:\t\t"+db_phone);
		            System.out.println("Sign-On date:\t"+db_signon_date);
		            System.out.println("Gender:\t\t"+db_gender);
		            System.out.println("Info public:\t"+db_info_agree);
		            System.out.println("=======================================\n");
		            
		            sendId = db_id;
					
				} else {
					System.out.println("Wrong password\n");			//패스워드가 틀렸다고 출력
				}
				
			} else {
				System.out.println("No ID record\n");
			}
					
		} catch(ClassNotFoundException e) {
			System.out.println("드라이버 로딩 실패");
		} catch(SQLException e) {
			System.out.println("에러" + e);
		} finally {
			try {
				if(conn != null) {
					conn.close();
				}
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
		return sendId;
		
	}

	public boolean updateInfo() {		//정보 수정 메소드 시작
		String record=null, sqlModify, field=null, input_id=null;					//문자열 변수 userId 선언
		int selectNum = 0;
		
		System.out.print("\n========= Update Info =========");
		input_id = userLogIn();
		
		if(input_id != null) {
				
			System.out.println("=======================================");
			System.out.println("1. Id\n2. Password\n3. Name\n4. Birthday\n5. Email\n6. Nickname\n7. Phone\n8. Gender\n9. Info agreement");
			System.out.println("=======================================");
		
			System.out.print("- 수정할 항목을 선택세요: ");
			selectNum = sc.nextInt();	//정수를 입력받아 selectNum 변수에 저장
			System.out.print("- 수정사항을 입력하세요: ");
			
			switch(selectNum) {		//selectNum이 가지는 번호로 case 이동
			case 1:		//회원가입 
				record = sc.next();
				field = "id";
				System.out.println();
				break;
			case 2:		//로그인
				record = sc.next();
				field = "password";
				System.out.println();
				break;
			case 3:		//정보수정
				record = sc.next();
				field = "name";
				System.out.println();
				break;
			case 4:		//정보삭제
				record = sc.next();
				field = "birthday";
				System.out.println();
				break;
			case 5:		//종료
				record = sc.next();
				field = "email";
				System.out.println();
				break;
			case 6:
				record = sc.next();
				field = "nickname";
				System.out.println();
				break;
			case 7:
				record = sc.next();
				field = "phone";
				System.out.println();
				break;
			case 8:
				record = sc.next();
				field = "gender";
				System.out.println();
				break;
			case 9:
				record = sc.next();
				field = "info_agree";
				System.out.println();
				break;
			default:
				System.out.println("Error! - 1~10번 중에서 선택하세요.\n");
				break;
			}
	
			sqlModify = "update user set "+field+" = '"+record+"' where id = '"+input_id+"';";
			
			try {
				Class.forName("com.mysql.jdbc.Driver");
				conn = DriverManager.getConnection("jdbc:mysql://localhost/yonghojoon", "root", "sjrnfl12^^");
				System.out.println("연결 성공");
				stmt = conn.createStatement();
				
				stmt.executeUpdate(sqlModify);
				System.out.println("수정 완료\n");
				
				
			} catch(ClassNotFoundException e) {
				System.out.println("드라이버 로딩 실패");
			} catch(SQLException e) {
				System.out.println("에러" + e);
			} finally {
				try {
					if(conn != null) {
						conn.close();
					}
				} catch(SQLException e) {
					e.printStackTrace();
				}
			}
			
		} else {
			System.out.println("Log-in fail\n");
		}
		
		return true;
	}
	
	public void deleteInfo() {		//정보 삭제 메소드 시작
		String input_id, confirm;					//문자열 변수 userId 선언
	
		System.out.print("\n========= Delete Info =========");
		input_id = userLogIn();
		
		if(input_id != null) {
			System.out.print("Delete?(y/n): ");
			confirm = sc.next();
			
			if(confirm.equals("y")) {
				try {
					Class.forName("com.mysql.jdbc.Driver");
					conn = DriverManager.getConnection("jdbc:mysql://localhost/yonghojoon", "root", "sjrnfl12^^");
					System.out.println("연결 성공");
					stmt = conn.createStatement();
					
					String sqlDelete = "delete from user where id = '"+input_id+"';"; 
					stmt.executeUpdate(sqlDelete);
					System.out.println("정보 삭제 완료\n");
					
					
				} catch(ClassNotFoundException e) {
					System.out.println("드라이버 로딩 실패");
				} catch(SQLException e) {
					System.out.println("에러" + e);
				} finally {
					try {
						if(conn != null) {
							conn.close();
						}
					} catch(SQLException e) {
						e.printStackTrace();
					}
				}
			} 
			
		} else {
			System.out.println("삭제 취소\n");
		}
			
	}
     
}
