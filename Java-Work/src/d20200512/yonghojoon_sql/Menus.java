package d20200512.yonghojoon_sql;

import java.util.Scanner;

class Menus {
	
	public void mainMenu() {		//mainMenu 메소드 시작
		
		int selectNum = 0;
		
		Scanner sc = new Scanner(System.in);
		ProcessSql sql = new ProcessSql();
		
		while(selectNum != 5) {		//selectNum 에 5가 입력되면 반복문 종료
			
			System.out.println("===== 회원관리 프로그램 Ver.0.1.0 =====\n1. 가입\n2. 로그인\n3. 정보수정\n4. 정보삭제\n5. 종료");
			System.out.println("=======================================");
			System.out.print("- 당신이 원하는 메뉴를 선택세요: ");
	
			selectNum = sc.nextInt();	//정수를 입력받아 selectNum 변수에 저장
			
			switch(selectNum) {		//selectNum이 가지는 번호로 case 이동
			case 1:		//회원가입
				sql.setInfo();	//setInfo 메소드 호출
				break;
			case 2:		//로그인
				sql.userLogIn();	//userLogIn 메소드 호출
				break;
			case 3:		//정보수정
				sql.updateInfo();	//changeUser 메소드 호출
				break;
			case 4:		//정보삭제
				sql.deleteInfo();	//deleteUser 메소드 호출
				break;
			case 5:		//종료
				System.out.println("프로그램이 종료되었습니다.");	
				sc.close();
				break;
			default:
				System.out.println("Error! - 1~5번 중에서 선택하세요.\n");	//1~5 사이 값이 아니면 출력문 실행
				break;
			}
		}
		
	}
}